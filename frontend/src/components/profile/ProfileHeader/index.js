import React from 'react';
import { Avatar, Typography } from '@material-ui/core';
import { Place } from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';

const useStyles = makeStyles((theme) => ({
  root: {},

  header: {
    padding: theme.spacing(4),
    paddingBottom: theme.spacing(16),
    margin: -theme.spacing(4),
    marginBottom: -theme.spacing(8),
    backgroundColor: '#2a2a2a',
    color: 'white',
  },

  userDetail: {
    display: 'flex',
    alignItems: 'center',
  },

  avatar: {
    width: 60,
    height: 60,
    marginRight: theme.spacing(2),
  },

  place: {
    marginTop: theme.spacing(4),
    display: 'flex',
    alignItems: 'center',
  },
}));

const ProfileHeader = () => {
  const classes = useStyles();

  const { userData, loading } = useSelector(({ auth }) => auth);

  return (
    <div className={classes.root}>
      <div className={classes.header}>
        <div className={classes.userDetail}>
          <Avatar className={classes.avatar} />
          <Typography variant="h5">
            <p>Sergei Girshev</p>
          </Typography>
        </div>
        <div className={classes.place}>
          <Place />
          <Typography variant="body2">Zurich, Switzerland</Typography>
        </div>
      </div>
    </div>
  );
};

export default ProfileHeader;
