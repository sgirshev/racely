import React from 'react';
import { IconButton } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Facebook, Google, Strava } from 'components/Icons';
import theme from '../../styles/theme';
import { get } from 'immutable';

const useStyles = makeStyles((theme) => ({
  flexContainer: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
}));

const authProviders = ['google', 'facebook', 'strava'];

const getComponent = (type, isDisabled, path) => {
  let icon;
  switch (type) {
    case 'google':
      icon = (
        <Google
          fill={
            isDisabled
              ? theme.palette.text.disabled
              : theme.palette.primary.main
          }
        />
      );
      break;
    case 'facebook':
      icon = (
        <Facebook
          fill={
            isDisabled
              ? theme.palette.text.disabled
              : theme.palette.primary.main
          }
        />
      );
      break;
    case 'strava':
      icon = (
        <Strava
          fill={
            isDisabled
              ? theme.palette.text.disabled
              : theme.palette.primary.main
          }
        />
      );
      break;
    default:
    // do nothing
  }

  return (
    <IconButton href={`${path}${type}`} key={type} disabled={isDisabled}>
      {icon}
    </IconButton>
  );
};

const Oauth = ({ providers, path }) => {
  const classes = useStyles();

  return (
    <div className={classes.flexContainer}>
      {authProviders.map((provider) => {
        return getComponent(provider, !providers.includes(provider), path);
      })}
    </div>
  );
};

export default Oauth;
