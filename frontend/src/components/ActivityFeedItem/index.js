import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Card,
  CardHeader,
  CardMedia,
  CardContent,
  CardActions,
  Avatar,
  Typography,
  IconButton,
  Grid,
  Divider,
} from '@material-ui/core';
import {
  Favorite,
  Share,
  MoreVert,
  DirectionsRun,
  DirectionsBike,
  Pool,
} from '@material-ui/icons';
import moment from 'moment';
import momentDurationFormatSetup from 'moment-duration-format';
import qs from 'qs';

momentDurationFormatSetup(moment);

const useStyles = makeStyles((theme) => ({
  root: {
    marginBottom: 30,
  },
  media: {
    height: 0,
    paddingTop: '56.25%',
  },
  avatar: {
    backgroundColor: theme.palette.primary.main,
  },
  paper: {
    padding: theme.spacing(2),
    minWidth: theme.spacing(48),
    textAlign: 'left',
  },
  measures: {
    fontSize: '2rem',
  },
  note: {
    display: 'block',
    fontSize: '1rem',
  },
}));

const ActivityFeedItem = (props) => {
  const classes = useStyles();
  const {
    name,
    type,
    startDate,
    distance,
    movingTime,
    avgSpeed,
    hasHeartRate,
    avgHeartRate,
    mapPolyline,
  } = props;

  const activityIcon = (function () {
    switch (type) {
      case 'Run':
        return <DirectionsRun />;
      case 'Ride':
        return <DirectionsBike />;
      case 'Swim':
        return <Pool />;
      default:
        return 'A';
    }
  })();

  // const [map, setMap] = useState(null);
  // const [loading, setLoading] = useState(false);

  // useEffect(() => {
  //   const fetchMap = async () => {
  //     try {
  //       setLoading(true);
  //       const response = await axios.get(
  //         'https://maps.googleapis.com/maps/api/staticmap',
  //         {
  //           params: {
  //             size: '640x400',
  //             path: mapPolyline,
  //             key: 'AIzaSyB9h40x3eJ_T5F9kC4eila0gfHRMsFosBQ',
  //           },
  //         },
  //       );
  //
  //       const { status, data } = response;
  //       const blob = new Blob([data], { type: 'image/png' });
  //       const src = URL.createObjectURL(blob);
  //       setLoading(false);
  //
  //       if (status === 200) setMap(src);
  //     } catch (error) {
  //       setLoading(false);
  //       console.log(error);
  //     }
  //   };
  //   fetchMap();
  // }, []);

  const mapRenderSettings = {
    size: '640x400',
    scale: 2,
    path: `weight:3|color:0x082030|enc:${mapPolyline}`,
    key: 'AIzaSyB9h40x3eJ_T5F9kC4eila0gfHRMsFosBQ',
  };
  const mapUrl =
    'https://maps.googleapis.com/maps/api/staticmap?' +
    qs.stringify(mapRenderSettings);

  return (
    <Card className={classes.root}>
      <CardHeader
        avatar={
          <Avatar aria-label="recipe" className={classes.avatar}>
            {activityIcon}
          </Avatar>
        }
        action={
          <IconButton aria-label="settings">
            <MoreVert />
          </IconButton>
        }
        title={name}
        subheader={moment(startDate).format('dddd, MMMM Do YYYY, H:mm')}
      />

      {mapPolyline && <CardMedia className={classes.media} image={mapUrl} />}

      <CardContent>
        <Grid container>
          <Grid item xs>
            <div className={classes.paper}>
              <Typography variant="body2">Distance</Typography>
              <Typography variant="h2">
                {(distance / 1000).toFixed(2)}{' '}
                <span className={classes.measures}>km</span>
              </Typography>
            </div>
          </Grid>

          <Divider orientation="vertical" flexItem />

          <Grid item xs>
            <div className={classes.paper}>
              <Typography variant="body2">Time</Typography>
              <Typography variant="h2">
                {moment.duration(movingTime, 'seconds').format()}
              </Typography>
            </div>
          </Grid>
        </Grid>

        <Divider />

        <Grid container>
          <Grid item xs>
            <div className={classes.paper}>
              <Typography variant="body2">Average pace</Typography>
              <Typography variant="h2">
                {moment.duration(1000 / avgSpeed, 'seconds').format()}{' '}
                <span className={classes.measures}>min/km</span>
              </Typography>
            </div>
          </Grid>

          <Divider orientation="vertical" flexItem />

          <Grid item xs>
            <div className={classes.paper}>
              <Typography variant="body2">Average heartrate</Typography>
              <Typography variant="h2">
                {hasHeartRate ? (
                  Math.round(avgHeartRate)
                ) : (
                  <>
                    <span>-</span>
                    <span className={classes.note}>
                      no information available
                    </span>
                  </>
                )}
              </Typography>
            </div>
          </Grid>
        </Grid>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton aria-label="add to favorites">
          <Favorite />
        </IconButton>
        <IconButton aria-label="share">
          <Share />
        </IconButton>
      </CardActions>
    </Card>
  );
};

export default ActivityFeedItem;
