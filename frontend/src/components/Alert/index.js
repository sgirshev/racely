import React from 'react';
import { Snackbar } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  error: { backgroundColor: theme.palette.error.main },
  warning: { backgroundColor: theme.palette.warning.main },
  success: { backgroundColor: theme.palette.success.main },
  info: { backgroundColor: theme.palette.info.main },
}));

const Alert = ({ type, open, message }) => {
  const classes = useStyles();

  type = ['error', 'warning', 'success', 'info'].some((t) => t === type)
    ? type
    : 'info';

  return (
    <Snackbar
      ContentProps={{ className: classes[type] }}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={open}
      message={message}
    />
  );
};

export default Alert;
