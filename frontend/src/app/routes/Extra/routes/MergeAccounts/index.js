import React, { useState } from 'react';
import {
  Button,
  Typography,
  Stepper,
  Step,
  StepLabel,
  Paper,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import MergeAccountsSteps from './steps';

const useStyles = makeStyles((theme) => ({
  header: {
    padding: 20,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  content: {
    padding: 20,
  },
  stepper: {
    width: '100%',
    backgroundColor: 'inherit',
  },
  backButton: {
    marginRight: theme.spacing(1),
  },
  stepperNavigation: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
  },
}));

const MergeAccounts = (props) => {
  const classes = useStyles();

  const [stepIndex, setStepIndex] = useState(0);
  const [mainAccountId, setMainAccountId] = useState(0);
  const [steps, setSteps] = useState([
    {
      label: 'Sign in to your account',
      completed: false,
      buttonLabel: 'Choose merge options',
    },
    { label: 'Choose merge options', completed: false, buttonLabel: 'Merge!' },
    { label: 'Merge completed!', completed: false, buttonLabel: 'Finish' },
  ]);

  const { loading, authConflict, user: currentUser } = useSelector(
    ({ auth }) => auth,
  );
  if (!authConflict) {
    return <Redirect to={'/app/account/profile-information'} />;
  }
  const { existedUser, authMethods } = authConflict;

  const handleNext = () => {
    setStepIndex((prevStepIndex) => prevStepIndex + 1);
  };

  const handleBack = () => {
    setStepIndex((prevStepIndex) => prevStepIndex - 1);
  };

  const authStepProps = {
    ...props,
    loading,
    currentUser,
    existedUser,
    authMethods,
    setSteps,
    stepIndex,
  };

  const mergeOptionsStepProps = {
    ...props,
    currentUser,
    existedUser,
    setSteps,
    stepIndex,
    setMainAccountId,
  };

  const successStepProps = {
    ...props,
    existedUserToken: authConflict.mergeToken,
    mainAccountId,
  };

  return (
    <Paper>
      <div className={classes.header}>
        <Typography variant="h5">Merge accounts</Typography>
      </div>
      <div className={classes.content}>
        <div className={classes.stepper}>
          <Stepper activeStep={stepIndex} alternativeLabel>
            {steps.map((step) => (
              <Step key={step.label}>
                <StepLabel>{step.label}</StepLabel>
              </Step>
            ))}
          </Stepper>
        </div>
        <MergeAccountsSteps
          stepIndex={stepIndex}
          authStepProps={authStepProps}
          mergeOptionsStepProps={mergeOptionsStepProps}
          successStepProps={successStepProps}
        />
        <div className={classes.stepperNavigation}>
          <Button
            disabled={stepIndex === 0}
            onClick={handleBack}
            className={classes.backButton}
          >
            Back
          </Button>
          <Button
            variant="contained"
            color="primary"
            onClick={handleNext}
            disabled={!steps[stepIndex].completed}
          >
            {steps[stepIndex].buttonLabel}
          </Button>
        </div>
      </div>
    </Paper>
  );
};

export default MergeAccounts;
