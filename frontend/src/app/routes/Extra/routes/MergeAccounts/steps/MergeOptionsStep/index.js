import React, { useEffect, useState } from 'react';
import clsx from 'clsx';
import {
  Avatar,
  Button,
  CircularProgress,
  TextField,
  Typography,
  Radio,
  FormControlLabel,
  Checkbox,
} from '@material-ui/core';
import { Skeleton } from '@material-ui/lab';
import moment from 'moment';
import { Check } from '@material-ui/icons';
import Oauth from 'components/Oauth';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';
import useAuthForm from 'utils/hooks/useAuthForm';
import { setMergeToken, signIn } from 'store/actions/Auth';
import queryString from 'query-string';
import axios from 'axios';

const useStyles = makeStyles((theme) => ({
  flexDirectionColumn: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  flexDirectionRow: {
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
  sectionRoot: {
    paddingTop: theme.spacing(4),
  },
  authSection: {
    flexGrow: 1,
  },
  avatar: {
    width: 60,
    height: 60,
  },
  userName: {
    fontSize: '1em',
    margin: 0,
  },
  email: { fontSize: '0.85em', margin: 0, color: theme.palette.text.hint },
  created: { fontSize: '0.85em', margin: 0, color: theme.palette.text.hint },
  userDetail: {
    padding: theme.spacing(2),
  },
  form: {
    width: '100%',
  },

  consent: {
    padding: theme.spacing(4),
  },
}));

const MergeOptionsStep = ({
  currentUser,
  existedUser,
  setSteps,
  stepIndex,
  setMainAccountId,
}) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const [selectedAccount, setSelectedAccount] = useState(existedUser.id);
  const [consent, setConsent] = useState(false);

  const handleSelectAccount = (event) => {
    setSelectedAccount(event.target.value);
    setMainAccountId(event.target.value);
  };

  const handleConsentChecked = (event) => {
    const isChecked = event.target.checked;
    setConsent(isChecked);

    setSteps((prevState) => {
      const newState = [...prevState];
      newState[stepIndex].completed = isChecked;
      return newState;
    });
  };

  return (
    <>
      <div className={clsx(classes.flexDirectionRow, classes.sectionRoot)}>
        <div className={clsx(classes.flexDirectionColumn, classes.authSection)}>
          <div className={classes.flexDirectionColumn}>
            <Avatar
              src={currentUser.profileImageUrl}
              className={classes.avatar}
            />
            <Typography
              className={clsx(classes.flexDirectionColumn, classes.userDetail)}
            >
              <span className={classes.userName}>
                {currentUser.firstName} {currentUser.lastName}
              </span>
              <span className={classes.email}>{currentUser.email}</span>
              <span className={classes.created}>
                {'Account created: '}
                {moment(currentUser.created).format('MMMM Do YYYY')}
              </span>
            </Typography>
          </div>
          <div>
            <Radio
              checked={selectedAccount === currentUser.id}
              onChange={handleSelectAccount}
              value={currentUser.id}
              name="current user"
              color="primary"
            />
          </div>
        </div>

        <div className={clsx(classes.flexDirectionColumn, classes.authSection)}>
          <div className={classes.flexDirectionColumn}>
            <Avatar
              src={existedUser.profileImageUrl}
              className={classes.avatar}
            />
            <Typography
              className={clsx(classes.flexDirectionColumn, classes.userDetail)}
            >
              <span className={classes.userName}>
                {existedUser.firstName} {existedUser.lastName}
              </span>
              <span className={classes.email}>{existedUser.email}</span>
              <span className={classes.created}>
                {'Account created: '}
                {moment(existedUser.created).format('MMMM Do YYYY')}
              </span>
            </Typography>
          </div>
          <div>
            <Radio
              checked={selectedAccount === existedUser.id}
              onChange={handleSelectAccount}
              value={existedUser.id}
              name="existed user"
              color="primary"
            />
          </div>
        </div>
      </div>
      <div className={classes.consent}>
        <FormControlLabel
          control={
            <Checkbox
              checked={consent}
              onChange={handleConsentChecked}
              color="primary"
            />
          }
          label="I agree that by pressing Merge! one of the accounts will
          be deleted permanently and part of the data will be used for the new account"
        />
      </div>
    </>
  );
};

export default MergeOptionsStep;
