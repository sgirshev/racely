import React from 'react';
import AuthStep from './AuthStep';
import MergeOptionsStep from './MergeOptionsStep';
import SuccessStep from './SuccessStep';

const MergeAccountsSteps = ({
  stepIndex,
  authStepProps,
  mergeOptionsStepProps,
  successStepProps,
}) => {
  const getStepContent = (step) => {
    switch (step) {
      case 0:
        return <AuthStep {...authStepProps} />;
      case 1:
        return <MergeOptionsStep {...mergeOptionsStepProps} />;
      case 2:
        return <SuccessStep {...successStepProps} />;
    }
  };

  return getStepContent(stepIndex);
};

export default MergeAccountsSteps;
