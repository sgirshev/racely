import React, { useEffect, useState } from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';
import { setMergeToken } from 'store/actions/Auth';
import { showAlert, hideAlert } from 'store/actions/UI';
import queryString from 'query-string';
import axios from 'axios';
import Alert from 'components/Alert';
import UserDetails from './UserDetails';

const useStyles = makeStyles((theme) => ({
  flexDirectionRow: {
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
  sectionRoot: {
    paddingTop: theme.spacing(4),
  },
}));

const AuthStep = ({
  loading,
  currentUser,
  existedUser,
  authMethods,
  setSteps,
  stepIndex,
  location,
}) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { alert } = useSelector(({ ui }) => ui);

  const [validating, setValidating] = useState(false);
  const [isAuthorized, setAuthorized] = useState(false);

  useEffect(() => {
    if (alert.open) {
      setTimeout(() => {
        dispatch(hideAlert());
      }, 3000);
    }
  }, [alert]);

  useEffect(() => {
    if (location.search) {
      setValidating(true);

      const { merge_token: mergeToken, message } = queryString.parse(
        location.search,
      );
      window.history.replaceState(null, null, location.pathname);

      const validateToken = async () => {
        if (!mergeToken) {
          dispatch(
            showAlert({
              message,
              type: 'error',
            }),
          );
          setValidating(false);
          setAuthorized(false);
        } else {
          const response = await axios.get(
            'http://localhost:5000/api/auth/me',
            {
              headers: { Authorization: `Bearer ${mergeToken}` },
            },
          );
          if (response.data.user.id === existedUser.id) {
            dispatch(setMergeToken(mergeToken));
            dispatch(
              showAlert({
                message: 'Successful authorization',
                type: 'success',
              }),
            );
            setValidating(false);
            setAuthorized(true);
            setSteps((prevState) => {
              const newState = [...prevState];
              newState[stepIndex].completed = true;
              return newState;
            });
          }
        }
      };
      setTimeout(validateToken, 2000);
    }
  }, []);

  return (
    <div className={clsx(classes.flexDirectionRow, classes.sectionRoot)}>
      <Alert open={alert.open} message={alert.message} type={alert.type} />

      <UserDetails
        user={currentUser}
        authMethods={authMethods}
        isAuthorized={true}
        validating={false}
        loading={loading}
      />
      <UserDetails
        user={existedUser}
        authMethods={authMethods}
        isAuthorized={isAuthorized}
        validating={validating}
        loading={loading}
      />

      <Alert open={alert.open} message={alert.message} type={alert.type} />
    </div>
  );
};

export default AuthStep;
