import React from 'react';
import { useDispatch } from 'react-redux';
import useAuthForm from 'utils/hooks/useAuthForm';
import { signIn } from 'store/actions/Auth';
import { Check } from '@material-ui/icons';
import { Button, TextField, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import Oauth from 'components/Oauth';

const useStyles = makeStyles((theme) => ({
  flexDirectionColumn: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  flexDirectionRow: {
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
  sectionRoot: {
    paddingTop: theme.spacing(4),
  },
  authSection: {
    flexGrow: 1,
  },
  avatar: {
    width: 60,
    height: 60,
  },
  userName: {
    fontSize: '1em',
    margin: 0,
  },
  email: { fontSize: '0.85em', margin: 0, color: theme.palette.text.hint },
  created: { fontSize: '0.85em', margin: 0, color: theme.palette.text.hint },
  userDetail: {
    padding: theme.spacing(2),
  },
  form: {
    width: '100%',
  },
  submit: {
    margin: theme.spacing(3, 'auto'),
  },
}));

const AuthForm = ({ isAuthorized, authMethods, loading, userId }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const { payload, errors, handleChange, handleSubmit } = useAuthForm(() => {
    dispatch(signIn(payload));
  });

  return isAuthorized ? (
    <Check />
  ) : (
    <form className={classes.form} noValidate onSubmit={handleSubmit}>
      <TextField
        onChange={handleChange}
        error={!!errors.password}
        helperText={errors.password || ''}
        margin="normal"
        required
        fullWidth
        name="password"
        label="Password"
        type="password"
        id="password"
        autoComplete="current-password"
      />

      <Button
        type="submit"
        fullWidth
        variant="contained"
        color="primary"
        className={classes.submit}
        disabled={loading}
      >
        Sign In
      </Button>
      <Typography variant="body2" align="center">
        or choose available sign in options:
      </Typography>
      <Oauth
        providers={authMethods}
        path={`http://localhost:5000/api/auth/authorize/${userId}/`}
      />
    </form>
  );
};

export default AuthForm;
