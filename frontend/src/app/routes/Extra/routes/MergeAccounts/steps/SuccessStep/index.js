import React, { useState, useEffect } from 'react';
import api from 'utils/api';
import { useDispatch } from 'react-redux';
import { HighlightOffRounded, CheckCircleOutline } from '@material-ui/icons';

import {
  clearAccountConflict,
  setAuthCredentials,
  setUserData,
} from 'store/actions/Auth';
import { Button, CircularProgress, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  flexDirectionColumn: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
}));

const SuccessStep = ({ existedUserToken, mainAccountId, history }) => {
  const classes = useStyles();

  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [success, setSuccess] = useState(false);

  useEffect(() => {
    const mergeRequest = async () => {
      setLoading(true);
      const response = await api.post('/auth/merge', {
        existedUserToken,
        mainAccountId,
      });

      if (response.data.success) {
        setSuccess(true);
        const { user, credentials } = response.data;
        dispatch(setAuthCredentials(credentials));
        dispatch(setUserData(user));
      } else {
        setSuccess(false);
      }
      setLoading(false);
    };

    mergeRequest();
  }, []);

  const handleSuccessRedirect = () => {
    history.push('/app/main/dashboard');
    localStorage.removeItem('authConflict');
    dispatch(clearAccountConflict());
  };

  return (
    <div className={classes.flexDirectionColumn}>
      {loading ? (
        <CircularProgress />
      ) : success ? (
        <div className={classes.flexDirectionColumn}>
          <CheckCircleOutline />
          <Typography variant="body2">Merge account succeeded!</Typography>
          <Button
            onClick={handleSuccessRedirect}
            variant="contained"
            color="primary"
          >
            Go to dashboard
          </Button>
        </div>
      ) : (
        <div className={classes.flexDirectionColumn}>
          <HighlightOffRounded />
          <Typography variant="body2">Merge account failed!</Typography>
          <Button
            onClick={() => history.push('/app/extra/merge-accounts#step=0')}
            variant="contained"
            color="primary"
          >
            Try again
          </Button>
        </div>
      )}
    </div>
  );
};

export default SuccessStep;
