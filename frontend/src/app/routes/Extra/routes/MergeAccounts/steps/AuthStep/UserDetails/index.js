import React from 'react';
import clsx from 'clsx';
import { Avatar, Typography, CircularProgress } from '@material-ui/core';
import moment from 'moment';
import { Check } from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';
import { Skeleton } from '@material-ui/lab';
import AuthForm from './AuthForm';

const useStyles = makeStyles((theme) => ({
  flexDirectionColumn: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  authSection: {
    flexGrow: 1,
  },
  avatar: {
    width: 60,
    height: 60,
  },
  userName: {
    fontSize: '1em',
    margin: 0,
  },
  email: { fontSize: '0.85em', margin: 0, color: theme.palette.text.hint },
  created: { fontSize: '0.85em', margin: 0, color: theme.palette.text.hint },
  userDetail: {
    padding: theme.spacing(2),
  },
}));

const UserDetails = ({
  user,
  isAuthorized,
  loading,
  authMethods,
  validating,
}) => {
  const classes = useStyles();

  if (!user) {
    return (
      <div className={clsx(classes.flexDirectionColumn, classes.authSection)}>
        <div className={classes.flexDirectionColumn}>
          <Skeleton variant="circle" className={classes.avatar} />
          <div
            className={clsx(classes.flexDirectionColumn, classes.userDetail)}
          >
            <Skeleton variant="text" width={280} />
            <Skeleton variant="text" width={240} />
            <Skeleton variant="text" width={180} />
          </div>
        </div>
      </div>
    );
  }

  return (
    <div className={clsx(classes.flexDirectionColumn, classes.authSection)}>
      <div className={classes.flexDirectionColumn}>
        <Avatar src={user.profileImageUrl} className={classes.avatar} />
        <Typography
          className={clsx(classes.flexDirectionColumn, classes.userDetail)}
        >
          <span className={classes.userName}>
            {user.firstName} {user.lastName}
          </span>
          <span className={classes.email}>{user.email}</span>
          <span className={classes.created}>
            {'Account created: '}
            {moment(user.created).format('MMMM Do YYYY')}
          </span>
        </Typography>
      </div>
      <div>
        {validating ? (
          <CircularProgress />
        ) : (
          <AuthForm
            isAuthorized={isAuthorized}
            authMethods={authMethods}
            loading={loading}
            userId={user.id}
          />
        )}
      </div>
    </div>
  );
};

export default UserDetails;
