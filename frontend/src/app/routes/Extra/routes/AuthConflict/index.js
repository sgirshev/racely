import React, { useEffect, useState } from 'react';
import queryString from 'query-string';
import {
  Button,
  Dialog,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Avatar,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  IconButton,
} from '@material-ui/core';
import { Close } from '@material-ui/icons';
import { Skeleton } from '@material-ui/lab';
import { makeStyles } from '@material-ui/core/styles';
import api from 'utils/api';
import moment from 'moment';
import { setAuthConflict } from 'store/actions/Auth';
import { useDispatch } from 'react-redux';
import { Redirect } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  avatar: {
    width: 60,
    height: 60,
    marginRight: theme.spacing(3),
  },
  userName: {
    fontSize: '1em',
    margin: 0,
  },
  email: { fontSize: '0.85em', margin: 0, color: theme.palette.text.hint },
  created: { fontSize: '0.85em', margin: 0, color: theme.palette.text.hint },
  dialogTitle: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
}));

const AuthConflict = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const [existedUser, setExistedUser] = useState(null);
  const [showButton, setShowButton] = useState(false);

  useEffect(() => {
    if (props.location.search) {
      const { existed_user: existedUserId, methods } = queryString.parse(
        props.location.search,
      );
      window.history.replaceState(null, null, props.location.pathname);

      const fetchAndSetAuthData = async () => {
        const response = await api.get(`/athlete/${existedUserId}`);
        setExistedUser(response.data.userData);

        const authConflictData = {
          existedUser: response.data.userData,
          authMethods: methods.split(','),
        };
        dispatch(setAuthConflict(authConflictData));
        localStorage.setItem('authConflict', JSON.stringify(authConflictData));
      };

      fetchAndSetAuthData();
    }
  }, []);

  if (!props.location.search)
    return <Redirect to={'/app/account/profile-information'} />;

  return (
    <Dialog open>
      <div className={classes.dialogTitle}>
        <DialogTitle>Authentication conflict</DialogTitle>
        <IconButton
          onClick={() => props.history.push('/app/account/profile-information')}
        >
          <Close />
        </IconButton>
      </div>
      <DialogContent>
        <DialogContentText variant="body1">
          We found that you already have another Racely account with Strava
          connected to it. We can merge all your accounts so you could sign in
          using any authentication provider. Please confirm that the account
          below is yours by signing in:
        </DialogContentText>

        {!existedUser ? (
          <List>
            <ListItem>
              <ListItemAvatar>
                <Skeleton variant="circle" width={60} height={60} />
              </ListItemAvatar>
              <ListItemText>
                <Skeleton variant="text" width={180} />
                <Skeleton variant="text" width={240} />
                <Skeleton variant="text" width={200} />
              </ListItemText>
            </ListItem>
          </List>
        ) : (
          <List
            onMouseEnter={() => setShowButton(true)}
            onMouseLeave={() => setShowButton(false)}
          >
            <ListItem button>
              <ListItemAvatar>
                <Avatar
                  src={existedUser.profileImageUrl}
                  className={classes.avatar}
                />
              </ListItemAvatar>
              <ListItemText>
                <p className={classes.userName}>
                  {existedUser.firstName} {existedUser.lastName}
                </p>
                <p className={classes.email}>{existedUser.email}</p>
                <p className={classes.created}>
                  {'Account created: '}
                  {moment(existedUser.created).format('MMMM Do YYYY')}
                </p>
              </ListItemText>
              {showButton && (
                <Button
                  onClick={() =>
                    props.history.push('/app/extra/merge-accounts#step=0')
                  }
                  variant="contained"
                  color="primary"
                >
                  Merge
                </Button>
              )}
            </ListItem>
          </List>
        )}
      </DialogContent>
    </Dialog>
  );
};

export default AuthConflict;
