import React from 'react';
import { Route, Switch } from 'react-router-dom';
import AuthConflict from './routes/AuthConflict';
import MergeAccounts from './routes/MergeAccounts';

const Main = ({ match }) => (
  <div>
    <Switch>
      <Route path={`${match.url}/auth-conflict`} component={AuthConflict} />
      <Route path={`${match.url}/merge-accounts`} component={MergeAccounts} />
    </Switch>
  </div>
);

export default Main;
