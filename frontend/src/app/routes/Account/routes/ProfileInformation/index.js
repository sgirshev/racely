import React from 'react';
import ProfileHeader from 'components/profile/ProfileHeader';
import { Grid, Paper, Tab, Tabs, Typography } from '@material-ui/core';
import clsx from 'clsx';
import StravaAccount from './StravaAccount';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';
import withPlaceholder from 'utils/hoc/withPlaceholder';

const useStyles = makeStyles((theme) => ({
  root: {},

  header: {
    padding: theme.spacing(4),
    paddingBottom: theme.spacing(16),
    margin: -theme.spacing(4),
    marginBottom: -theme.spacing(8),
    backgroundColor: '#2a2a2a',
    color: 'white',
  },

  userDetail: {
    display: 'flex',
    alignItems: 'center',
  },

  avatar: {
    width: 60,
    height: 60,
    marginRight: theme.spacing(2),
  },

  place: {
    marginTop: theme.spacing(4),
    display: 'flex',
    alignItems: 'center',
  },

  card: {
    padding: 0,
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },

  fixedHeight: {
    height: 360,
  },

  statistics: {
    height: 680,
  },

  recentActivities: {
    height: 406,
  },

  cardHeader: {
    height: theme.spacing(7),
    borderBottom: `1px ${'#e0e0e0'} solid`,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '0 32px',
  },

  gridContainer: {},
}));

const ProfileInformation = () => {
  const classes = useStyles();

  const { user, loading } = useSelector(({ auth }) => auth);

  const AsyncStravaAccount = withPlaceholder(StravaAccount);

  return (
    <div>
      <ProfileHeader />
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Paper className={clsx(classes.card, classes.fixedHeight)}>
            <div className={classes.cardHeader}>
              <Typography variant="body1">About</Typography>
              <div>
                <Tabs>
                  <Tab label="General" />
                  <Tab label="Personal" disabled />
                  <Tab label="Training" disabled />
                </Tabs>
              </div>
            </div>
          </Paper>
        </Grid>

        <Grid item xs={8}>
          <Paper className={clsx(classes.card, classes.statistics)}>
            <div>
              <Typography variant="body1">Statistics</Typography>
            </div>
          </Paper>
        </Grid>

        <Grid item xs container spacing={3}>
          <Grid item xs={12}>
            <AsyncStravaAccount loading={loading} data={user} />
          </Grid>
          <Grid item xs={12}>
            <Paper className={clsx(classes.card, classes.recentActivities)}>
              <div>
                <Typography variant="body1">Recent activities</Typography>
              </div>
            </Paper>
          </Grid>
        </Grid>

        <Grid item xs={4}>
          <Paper className={clsx(classes.card, classes.fixedHeight)}>
            <div>
              <Typography variant="body1">My gear</Typography>
            </div>
          </Paper>
        </Grid>

        <Grid item xs>
          <Paper className={clsx(classes.card, classes.fixedHeight)}>
            <div>
              <Typography variant="body1">Goals</Typography>
            </div>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
};

export default ProfileInformation;
