import React, { useEffect } from 'react';
import clsx from 'clsx';
import { Strava } from 'components/Icons';
import { Button, Paper, Typography, Link } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';

const useStyles = makeStyles((theme) => ({
  card: {
    padding: 0,
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },

  strava: {
    height: 250,
    padding: theme.spacing(3),
  },

  connectStravaHeader: {
    display: 'flex',
    alignItems: 'center',
  },

  connectStravaContent: {
    position: 'relative',
    height: '100%',
    width: '100%',
  },

  connected: {
    position: 'absolute',
    top: '40%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',

    '& a:first-of-type': {
      marginTop: 8,
    },
  },
}));

const StravaAccount = ({ data }) => {
  const classes = useStyles();

  const { location } = useSelector(({ router }) => router);
  const { user } = useSelector(({ auth }) => auth);

  useEffect(() => {
    localStorage.removeItem('initUrl');
  });

  const handleConnect = async () => {
    localStorage.setItem('initUrl', location.pathname);
    window.location.replace(
      `http://localhost:5000/api/auth/connect/strava/${user.id}`,
    );
  };

  return (
    <div>
      <Paper className={clsx(classes.card, classes.strava)}>
        <div className={classes.connectStravaHeader}>
          <Strava
            fill="primary "
            style={{ fontSize: 48 }}
            viewBox="0 0 32 32"
          />
          <Typography variant="body1">Strava account</Typography>
        </div>
        <div className={classes.connectStravaContent}>
          {data.stravaId ? (
            <div className={classes.connected}>
              <Button variant="contained" color="primary" disabled>
                Connected
              </Button>
              <Link
                href={`https://www.strava.com/athletes/47032487`}
                variant="caption"
              >
                View profile
              </Link>
              <Link href="#" variant="caption">
                Disconnect
              </Link>
            </div>
          ) : (
            <div className={classes.connected}>
              <Button
                variant="contained"
                color="primary"
                onClick={handleConnect}
              >
                Connect
              </Button>
            </div>
          )}
        </div>
      </Paper>
    </div>
  );
};

export default StravaAccount;
