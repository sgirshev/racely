import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import asyncComponent from 'utils/AsyncComponent';

const Account = ({ match }) => (
  <div>
    <Switch>
      <Redirect
        exact
        from={`${match.url}/`}
        to={`${match.url}/profile-information`}
      />
      <Route
        path={`${match.url}/profile-information`}
        component={asyncComponent(() => import('./routes/ProfileInformation'))}
      />
      <Route
        path={`${match.url}/settings`}
        component={asyncComponent(() => import('./routes/Settings'))}
      />
    </Switch>
  </div>
);

export default Account;
