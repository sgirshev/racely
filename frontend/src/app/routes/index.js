import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { withRouter } from 'react-router';
import Main from './Main';
import Account from './Account';
import Extra from './Extra';

const Routes = ({ match }) => (
  <Switch>
    <Route path={`${match.url}/main`} component={Main} />
    <Route path={`${match.url}/account`} component={Account} />
    <Route path={`${match.url}/extra`} component={Extra} />
  </Switch>
);

export default withRouter(Routes);
