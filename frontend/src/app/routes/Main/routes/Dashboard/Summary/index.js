import React from 'react';
import { Paper, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { ResponsiveBar } from '@nivo/bar';
import moment from 'moment';
import clsx from 'clsx';

const useStyles = makeStyles((theme) => ({
  header: {
    padding: 20,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  content: {
    padding: 20,
  },
  flexContainer: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  chart: {
    height: 100,
    width: '66%',
  },
  stats: {
    paddingTop: theme.spacing(6),
  },
  toolTip: {
    backgroundColor: 'white',
    padding: theme.spacing(),
    color: theme.palette.primary.main,
  },
}));

const Summary = (props) => {
  const classes = useStyles();
  const activities = props.data;

  const createSummaryData = () => {
    const reducerCallback = (summary, activity) => {
      const key = moment(activity.start_date_local).format('MMM');
      if (summary.byMonth[key] === undefined) {
        summary.byMonth[key] = {
          Running: 0,
          Cycling: 0,
          Swimming: 0,
        };
      }

      switch (activity.type) {
        case 'Run':
          summary.byMonth[key].Running++;
          summary.byActivity.Running++;
          break;
        case 'Ride':
          summary.byMonth[key].Cycling++;
          summary.byActivity.Cycling++;

          break;
        case 'Swim':
          summary.byMonth[key].Swimming++;
          summary.byActivity.Swimming++;
          break;
        default:
        // skip
      }
      return summary;
    };

    return activities.reduce(reducerCallback, {
      byMonth: {},
      byActivity: { Running: 0, Cycling: 0, Swimming: 0 },
    });
  };

  const data = {};
  data.totalActivities = activities.length;
  data.activitiesSummary = createSummaryData();

  const chartData = [];

  for (const [month, activitiesData] of Object.entries(
    data.activitiesSummary.byMonth,
  )) {
    chartData.push({ Month: month, ...activitiesData });
  }

  const toolTip = (props) => {
    return (
      <div className={classes.toolTip}>
        <Typography variant="body2">
          <strong>Running: {props.data.Running}</strong>
        </Typography>
        <Typography variant="body2">
          <strong>Cycling: {props.data.Cycling}</strong>
        </Typography>
        <Typography variant="body2">
          <strong>Swimming: {props.data.Swimming}</strong>
        </Typography>
      </div>
    );
  };

  return (
    <Paper {...props}>
      <div className={classes.header}>
        <Typography variant="h5">Year summary</Typography>
      </div>
      <div className={classes.content}>
        <div className={classes.flexContainer}>
          <div>
            <Typography variant="body2">Total activities</Typography>
            <Typography variant="h2">{data.totalActivities}</Typography>
          </div>
          <div className={classes.chart}>
            <ResponsiveBar
              data={chartData}
              keys={['Running', 'Cycling', 'Swimming']}
              indexBy="Month"
              colors={['#94090d', '#ff1d23', '#d40d12']}
              enableLabel={false}
              enableGridX={false}
              enableGridY={false}
              margin={{ top: 0, right: 0, bottom: 20, left: 0 }}
              axisTop={null}
              axisRight={null}
              axisBottom={{
                tickSize: 0,
                tickPadding: 5,
              }}
              axisLeft={null}
              animate={true}
              tooltip={toolTip}
            />
          </div>
        </div>
        <div className={clsx(classes.flexContainer, classes.stats)}>
          <div>
            <Typography variant="body2">Running</Typography>
            <Typography variant="h5">
              {data.activitiesSummary.byActivity.Running}
            </Typography>
          </div>
          <div>
            <Typography variant="body2">Cycling</Typography>
            <Typography variant="h5">
              {data.activitiesSummary.byActivity.Cycling}
            </Typography>
          </div>
          <div>
            <Typography variant="body2">Swimming</Typography>
            <Typography variant="h5">
              {data.activitiesSummary.byActivity.Swimming}
            </Typography>
          </div>
        </div>
      </div>
    </Paper>
  );
};

export default Summary;
