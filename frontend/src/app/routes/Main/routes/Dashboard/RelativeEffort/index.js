import React from 'react';
import { Paper } from '@material-ui/core';

const RelativeEffort = (props) => {
  return <Paper {...props}>15</Paper>;
};

export default RelativeEffort;
