import React, { useState } from 'react';
import { Paper, Collapse, IconButton, Typography } from '@material-ui/core';
import { ExpandMore, Close } from '@material-ui/icons';
import api from 'utils/api';

import HeartRateChart from './HeartRateChart';
import Details from './Details';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

const useStyles = makeStyles((theme) => ({
  header: {
    padding: 20,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  content: {
    padding: 20,
  },
  cardActions: {
    width: '100%',
    height: 80,
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
}));

const HeartRateZones = (props) => {
  const [expanded, setExpanded] = useState(false);
  const [activityData, setActivityData] = useState(null);
  const [loading, setLoading] = useState(false);

  const classes = useStyles();
  const activities = props.data;
  const age = 30;

  const getChartData = () => {
    const avgHRData = [];
    const maxHRData = [];

    activities.forEach((activity) => {
      avgHRData.push({
        x: new Date(activity.start_date_local),
        y: activity.average_heartrate
          ? Math.round(activity.average_heartrate)
          : Math.round((220 - age) * 0.7),
        type: activity.type,
        id: activity.id,
      });
      maxHRData.push({
        x: new Date(activity.start_date_local),
        y: activity.max_heartrate
          ? Math.round(activity.max_heartrate)
          : Math.round((220 - age) * 0.8),
        type: activity.type,
        id: activity.id,
      });
    });

    const avgHR = {
      id: 'Average heart rate',

      data: avgHRData,
    };
    const maxHR = {
      id: 'Maximum heart rate',
      data: maxHRData,
    };
    return [avgHR, maxHR];
  };

  const handleExpandClick = () => {
    setExpanded((currentExpandedState) => {
      if (currentExpandedState) {
        setExpanded(false);
        setActivityData(null);
      } else {
        setExpanded(true);
      }
    });
  };

  const handleChartClick = async ({ data }) => {
    setExpanded(true);

    const response = await fetchActivityData(data.id);
    const { success, activity, stream } = response;

    if (success) {
      setLoading(false);
      setActivityData({ activity, stream });
    }
  };

  const fetchActivityData = async (id) => {
    setLoading(true);
    try {
      const activityDataResponse = await api.get(`activities/${id}`);
      const activityStreamsResponse = await api.get(
        `activities/${id}/streams`,
        {
          params: { keys: 'heartrate,time,distance', key_by_type: true },
        },
      );
      if (
        activityDataResponse.status === 200 &&
        activityStreamsResponse.status === 200
      ) {
        return {
          success:
            activityDataResponse.data.success &&
            activityStreamsResponse.data.success,
          activity: activityDataResponse.data.activity,
          stream: activityStreamsResponse.data.stream,
        };
      } else {
        return { success: false };
      }
    } catch (error) {
      setLoading(false);
      return error;
    }
  };

  return (
    <Paper {...props}>
      <div className={classes.header}>
        <Typography variant="h5">Heart rate analysis</Typography>
        <IconButton
          className={clsx(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={handleExpandClick}
        >
          {!expanded ? <ExpandMore /> : <Close />}
        </IconButton>
      </div>
      <div className={classes.content}>
        <Collapse in={expanded} timeout="auto" unmountOnExit>
          <Details activityData={activityData} loading={loading} />
        </Collapse>
        <HeartRateChart
          age={age}
          data={getChartData()}
          handleChartClick={handleChartClick}
        />
      </div>
    </Paper>
  );
};

export default HeartRateZones;
