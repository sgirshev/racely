import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Typography } from '@material-ui/core';
import { Skeleton } from '@material-ui/lab';
import { InfoOutlined } from '@material-ui/icons';
import moment from 'moment';
import momentDurationFormatSetup from 'moment-duration-format';
import HeartRateChartDetailed from './HeartRateChartDetailed';

momentDurationFormatSetup(moment);

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    display: 'flex',
  },
  paper: {
    padding: 8,
  },
}));

const Details = ({ activityData, loading }) => {
  const classes = useStyles();

  const getPlaceholderWidth = () => {
    return Math.floor(Math.random() * 80 + 80);
  };

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Typography variant="h5">
            {loading ? (
              <Skeleton width={getPlaceholderWidth()} />
            ) : (
              activityData && activityData.activity.name
            )}
          </Typography>
        </Grid>

        <Grid item xs={6}>
          <div className={classes.paper}>
            <Typography variant="body2">Type</Typography>
            <Typography variant="h5">
              {loading ? (
                <Skeleton width={getPlaceholderWidth()} />
              ) : (
                activityData && activityData.activity.type
              )}
            </Typography>
          </div>
        </Grid>

        <Grid item xs={6}>
          <div className={classes.paper}>
            <Typography variant="body2">Moving time</Typography>
            <Typography variant="h5">
              {loading ? (
                <Skeleton width={getPlaceholderWidth()} />
              ) : (
                activityData &&
                moment
                  .duration(activityData.activity.moving_time, 'seconds')
                  .format('h [h] m [m] s [s]')
              )}
            </Typography>
          </div>
        </Grid>

        <Grid item xs={6}>
          <div className={classes.paper}>
            <Typography variant="body2">Average pace</Typography>
            <Typography variant="h5">
              {loading ? (
                <Skeleton width={getPlaceholderWidth()} />
              ) : (
                activityData &&
                moment
                  .duration(
                    1000 / activityData.activity.average_speed,
                    'seconds',
                  )
                  .format() + ' min/km'
              )}
            </Typography>
          </div>
        </Grid>

        <Grid item xs={6}>
          <div className={classes.paper}>
            <Typography variant="body2">Maximum pace</Typography>
            <Typography variant="h5">
              {loading ? (
                <Skeleton width={getPlaceholderWidth()} />
              ) : (
                activityData &&
                moment
                  .duration(1000 / activityData.activity.max_speed, 'seconds')
                  .format() + ' min/km'
              )}
            </Typography>
          </div>
        </Grid>

        <Grid item xs={6}>
          <div className={classes.paper}>
            <Typography variant="body2">Average heartrate</Typography>
            <Typography variant="h5">
              {loading ? (
                <Skeleton width={getPlaceholderWidth()} />
              ) : activityData && activityData.activity.has_heartrate ? (
                Math.round(activityData.activity.average_heartrate) + ' bpm'
              ) : (
                <span>-</span>
              )}
            </Typography>
          </div>
        </Grid>

        <Grid item xs={6}>
          <div className={classes.paper}>
            <Typography variant="body2">Maximum heartrate</Typography>
            <Typography variant="h5">
              {loading ? (
                <Skeleton width={getPlaceholderWidth()} />
              ) : activityData && activityData.activity.has_heartrate ? (
                Math.round(activityData.activity.max_heartrate) + ' bpm'
              ) : (
                <span>-</span>
              )}
            </Typography>
          </div>
        </Grid>
      </Grid>

      <Grid container spacing={3}>
        <Grid item xs={12}>
          <div className={classes.paper}>
            {activityData ? (
              activityData.stream.heartrate === undefined ? (
                <div style={{ display: 'flex', alignItems: 'center' }}>
                  <InfoOutlined />
                  <Typography variant="body2">
                    No heart rate data available
                  </Typography>
                </div>
              ) : (
                <HeartRateChartDetailed stream={activityData.stream} />
              )
            ) : (
              <Typography variant="body2">
                Please, click on the chart to select activity
              </Typography>
            )}
          </div>
        </Grid>
      </Grid>
    </div>
  );
};

export default Details;
