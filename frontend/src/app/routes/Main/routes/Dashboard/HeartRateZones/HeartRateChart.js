import React from 'react';
import { ResponsiveLine } from '@nivo/line';
import { area } from 'd3-shape';
import { makeStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  chartRoot: {
    backgroundColor: 'white',
    width: '100%',
    height: 400,
  },
  toolTip: {
    backgroundColor: 'white',
    padding: theme.spacing(),
    color: theme.palette.primary.main,
  },
}));

const HeartRateChart = ({ age, data, handleChartClick }) => {
  const classes = useStyles();

  const createZones = (age) => {
    const zoneAreas = [];
    const zones = [
      { factor: 0.6, color: '#ff1d23' },
      { factor: 0.7, color: '#d40d12' },
      { factor: 0.8, color: '#94090d' },
      { factor: 0.9, color: '#5c0002' },
      { factor: 1, color: '#450003' },
    ];
    zones.forEach((zone, index) => {
      zoneAreas.push(({ series, xScale, yScale }) => {
        const areaGenerator = area()
          .x((d) => xScale(d.data.x))
          .y0(() => yScale((220 - age) * (zone.factor - 0.1)))
          .y1(() => yScale((220 - age) * zone.factor));

        return (
          <path
            d={areaGenerator(series[0].data)}
            fill={zone.color}
            opacity={0.9}
          />
        );
      });
    });
    return zoneAreas;
  };

  const toolTip = (props) => {
    return (
      <div className={classes.toolTip}>
        <Typography variant="body2">
          <strong>Date: </strong>
          {props.point.data.xFormatted}
        </Typography>
        <Typography variant="body2">
          <strong>Type: </strong>
          {props.point.data.type}
        </Typography>
        <Typography variant="body2">
          <strong>{props.point.serieId}: </strong>
          {props.point.data.yFormatted}
        </Typography>
      </div>
    );
  };

  const minY = (220 - age) * 0.5;
  const maxY = 220 - age;

  return (
    <div className={classes.chartRoot}>
      <ResponsiveLine
        data={data}
        curve="basis"
        enableGridX={false}
        enableGridY={false}
        margin={{ top: 40, right: 20, bottom: 20, left: 24 }}
        xScale={{
          type: 'time',
          format: 'native',
          precision: 'day',
        }}
        xFormat="time:%b %d, %Y"
        yScale={{
          type: 'linear',
          min: minY,
          max: maxY,
        }}
        axisTop={null}
        axisRight={null}
        axisBottom={{
          tickSize: 0,
          tickPadding: 5,
          format: '%b %d',
          tickValues: 'every month',
        }}
        axisLeft={{
          tickSize: 0,
          tickPadding: 5,
        }}
        isInteractive={true}
        useMesh={true}
        enablePoints={false}
        enableArea={true}
        areaBaselineValue={minY}
        areaBlendMode="screen"
        areaOpacity={0.3}
        enableCrosshair={true}
        crosshairType="x"
        layers={[
          ...createZones(age),
          'grid',
          'markers',
          'axes',
          'areas',
          'slices',
          'crosshair',
          'lines',
          'points',
          'mesh',
          'legends',
        ]}
        onClick={handleChartClick}
        tooltip={toolTip}
      />
    </div>
  );
};

export default HeartRateChart;
