import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  Grid,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Summary from './Summary';
import HeartRateZones from './HeartRateZones';
import RelativeEffort from './RelativeEffort';
import { fetchActivities } from 'store/actions/Activities';
import withPlaceholder from 'utils/hoc/withPlaceholder';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  button: {
    '& a': { color: theme.palette.primary.main, textDecoration: 'none' },
  },
}));

const Dashboard = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { activities, loading } = useSelector(({ activities }) => activities);
  const { user } = useSelector(({ auth }) => auth);

  useEffect(() => {
    if (user && user.stravaId) {
      dispatch(fetchActivities('year'));
    }
  }, [dispatch, user]);

  const AsyncSummary = withPlaceholder(Summary);
  const AsyncRelativeEffort = withPlaceholder(RelativeEffort);
  const AsyncHeartRateZones = withPlaceholder(HeartRateZones);

  let dashboard = null;
  if (user) {
    if (user.stravaId) {
      dashboard = (
        <Grid container spacing={3}>
          <Grid item xs={12} md={4} lg={4}>
            <AsyncSummary
              loading={loading}
              data={activities}
              className={clsx(classes.card, loading && classes.placeholder)}
            />
          </Grid>
          <Grid item xs={12} md={8} lg={8}>
            <AsyncRelativeEffort
              loading={loading}
              data={activities}
              className={clsx(classes.card, loading && classes.placeholder)}
            />
          </Grid>
          <Grid item xs={12}>
            <AsyncHeartRateZones
              loading={loading}
              data={activities}
              className={clsx(classes.card, loading && classes.placeholder)}
            />
          </Grid>
        </Grid>
      );
    } else {
      dashboard = (
        <Dialog open>
          <DialogTitle>
            Connect your Strava account to get the information about your
            activities.
          </DialogTitle>
          <DialogContent>
            <DialogContentText variant="body1">
              You will be redirected to the Racely profile page where you will
              be able to connect to your Strava account. You will be asked to
              provide access to your profile and activities information, so that
              we can show you the training analytics.
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button color="primary" className={classes.button}>
              <Link to="/app/account/profile-information">Connect</Link>
            </Button>
          </DialogActions>
        </Dialog>
      );
    }
  }

  return <div>{dashboard}</div>;
};

export default Dashboard;
