import React, { useRef, useCallback, useState, useEffect } from 'react';
import ActivityFeedItem from 'components/ActivityFeedItem';
import { makeStyles } from '@material-ui/core/styles';
import { CircularProgress } from '@material-ui/core';
import api from 'utils/api';

const useStyles = makeStyles((theme) => ({
  activityFeedItems: {
    margin: '0 auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  progressBox: {},
}));

const ActivityFeed = () => {
  const classes = useStyles();

  const [element, setElement] = useState(null);
  const [activities, setActivities] = useState([]);
  const [loading, setLoading] = useState(false);

  const page = useRef(1);
  const prevY = useRef(0);

  const observer = useRef(
    new IntersectionObserver(
      (entries) => {
        const firstEntry = entries[0];
        const y = firstEntry.boundingClientRect.y;

        if (prevY.current > y) {
          loadMore();
        }

        prevY.current = y;
      },
      { threshold: 1 },
    ),
  );

  const fetchData = useCallback(async (page) => {
    setLoading(true);

    try {
      const response = await api.get(`athlete/activities/`, {
        params: { page: page.current, per_page: 5 },
      });
      const { status, data } = response;
      setLoading(false);
      return { status, activities: data.activities };
    } catch (error) {
      setLoading(false);
      return error;
    }
  }, []);

  const handleInitial = useCallback(
    async (page) => {
      const data = await fetchData(page);
      const { status, activities } = data;
      if (status === 200)
        setActivities((prevActivities) => [...prevActivities, ...activities]);
    },
    [fetchData],
  );

  const loadMore = () => {
    page.current++;
    handleInitial(page);
  };

  useEffect(() => {
    handleInitial(page);
  }, [handleInitial]);

  useEffect(() => {
    const currentElement = element;
    const currentObserver = observer.current;

    if (currentElement) {
      currentObserver.observe(currentElement);
    }

    return () => {
      if (currentElement) {
        currentObserver.unobserve(currentElement);
      }
    };
  }, [element]);

  return (
    <div className={classes.activityFeedItems}>
      {activities &&
        activities.map((activity, index) => {
          return (
            <ActivityFeedItem
              key={activity.id}
              name={activity.name}
              type={activity.type}
              startDate={activity.start_date_local}
              distance={activity.distance}
              movingTime={activity.moving_time}
              avgSpeed={activity.average_speed}
              maxSpeed={activity.max_speed}
              hasHeartRate={activity.has_heartrate}
              avgHeartRate={activity.average_heartrate}
              maxHeartRate={activity.max_heartrate}
              elevGain={activity.total_elevation_gain}
              mapPolyline={activity.map.summary_polyline}
            />
          );
        })}

      <div ref={setElement} className={classes.progressBox}>
        {loading && <CircularProgress />}
      </div>
    </div>
  );
};

export default ActivityFeed;
