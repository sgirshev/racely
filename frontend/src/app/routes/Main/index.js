import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import Dashboard from './routes/Dashboard';
import ActivityFeed from './routes/ActivityFeed';
import TrainingPlan from './routes/TrainingPlan';

const Main = ({ match }) => (
  <div>
    <Switch>
      <Redirect exact from={`${match.url}/`} to={`${match.url}/dashboard`} />
      <Route path={`${match.url}/dashboard`} component={Dashboard} />
      <Route path={`${match.url}/activity-feed`} component={ActivityFeed} />
      <Route path={`${match.url}/training-plan`} component={TrainingPlan} />
    </Switch>
  </div>
);

export default Main;
