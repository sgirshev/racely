const plugins = [
  [
    'babel-plugin-import',
    {
      libraryName: '@material-ui/Icons',
      libraryDirectory: 'esm',
      camel2DashComponentName: false,
    },
    'Icons',
  ],
  [
    'babel-plugin-import',
    {
      libraryName: '@material-ui/core',
      libraryDirectory: 'esm',
      camel2DashComponentName: false,
    },
    'core',
  ],
  ['react-hot-loader/babel'],
];

module.exports = { plugins };
