import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#324759',
      dark: '#082030',
      light: '#5d7286',
    },
    secondary: {
      main: '#66d9c4',
      dark: '#2ba794',
      light: '#9bfff7',
    },
    error: {
      main: '#8C425D',
    },
    warning: {
      main: '#BF637C',
    },
    success: {
      main: '#2ba794',
    },
    info: {
      main: '#324759',
    },
    background: {
      default: '#fafafa',
    },
    text: {
      primary: '#082030',
      secondary: '',
      disabled: '#9797a8',
      hint: '#5c5c5c',
    },
  },

  typography: {
    fontFamily: ['Roboto'],
  },
});

export default theme;
