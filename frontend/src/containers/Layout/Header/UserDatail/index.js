import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Avatar, Menu, MenuItem, Typography } from '@material-ui/core';
import { Skeleton } from '@material-ui/lab';
import { ArrowDropDown } from '@material-ui/icons';
import { useDispatch, useSelector } from 'react-redux';
import { signOut } from 'store/actions/Auth';

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    alignItems: 'center',
    cursor: 'pointer',
  },
}));

const UserDetail = () => {
  const classes = useStyles();
  const [menuOpen, setMenuOpen] = useState(false);
  const [anchorEl, setAnchorEl] = useState(false);
  const dispatch = useDispatch();

  const { user } = useSelector(({ auth }) => auth);

  const handleOpen = (event) => {
    setMenuOpen(true);
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => setMenuOpen(false);

  const userDataPlaceholder = (
    <div className={classes.container}>
      <Skeleton variant="text" width={120} />
      <Skeleton variant="circle" width={40} height={40} />
    </div>
  );

  return (
    <>
      {user ? (
        <div className={classes.container} onClick={handleOpen}>
          <Typography variant="body1">
            {user.firstName} {user.lastName}
          </Typography>
          <ArrowDropDown />
          <Avatar src={user.profileImageUrl} />
        </div>
      ) : (
        userDataPlaceholder
      )}

      <Menu open={menuOpen} anchorEl={anchorEl} onClose={handleClose}>
        <MenuItem
          onClick={() => {
            handleClose();
            dispatch(signOut());
          }}
        >
          Sign Out
        </MenuItem>
      </Menu>
    </>
  );
};

export default UserDetail;
