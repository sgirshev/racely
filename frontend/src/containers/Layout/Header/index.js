import React from 'react';
import clsx from 'clsx';
import { AppBar, Divider, Toolbar } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';
import UserDetail from './UserDatail';

const drawerWidth = 320;

const useStyles = makeStyles((theme) => ({
  header: {
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  headerShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  toolbar: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
}));

const Header = () => {
  const classes = useStyles();
  const { drawerMenuClosed } = useSelector(({ ui }) => ui);

  return (
    <AppBar
      color="inherit"
      position="absolute"
      elevation={0}
      className={clsx(classes.header, !drawerMenuClosed && classes.headerShift)}
    >
      <Toolbar className={classes.toolbar}>
        <UserDetail />
      </Toolbar>
      <Divider />
    </AppBar>
  );
};

export default Header;
