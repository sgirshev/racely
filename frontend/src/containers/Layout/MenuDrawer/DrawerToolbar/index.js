import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { IconButton } from '@material-ui/core';
import { toggleMenu } from 'store/actions/UI';
import { useDispatch, useSelector } from 'react-redux';
import { ChevronLeft, Menu } from '@material-ui/icons';

const useStyles = makeStyles((theme) => ({
  drawerToolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 4px',
    ...theme.mixins.toolbar,
  },
}));

const DrawerToolbar = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { drawerMenuClosed } = useSelector(({ ui }) => ui);

  const menuIcon = drawerMenuClosed ? <Menu /> : <ChevronLeft />;

  return (
    <div className={classes.drawerToolbar}>
      <IconButton onClick={() => dispatch(toggleMenu(!drawerMenuClosed))}>
        {menuIcon}
      </IconButton>
    </div>
  );
};

export default DrawerToolbar;
