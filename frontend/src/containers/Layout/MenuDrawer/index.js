import React from 'react';
import { Link } from 'react-router-dom';
import clsx from 'clsx';
import {
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  ListSubheader,
} from '@material-ui/core';
import {
  DynamicFeed,
  Dashboard,
  Today,
  Person,
  Settings,
} from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';
import DrawerToolbar from './DrawerToolbar';

const drawerWidth = 320;

const useStyles = makeStyles((theme) => ({
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
  },
}));

const mainListItems = (
  <div>
    <ListSubheader inset>MAIN</ListSubheader>
    <ListItem button component={Link} to="/app/main/dashboard">
      <ListItemIcon>
        <Dashboard />
      </ListItemIcon>
      <ListItemText primary="Dashboard" />
    </ListItem>
    <ListItem button component={Link} to="/app/main/activity-feed">
      <ListItemIcon>
        <DynamicFeed />
      </ListItemIcon>
      <ListItemText primary="Activity feed" />
    </ListItem>
    <ListItem button component={Link} to="/app/main/training-plan">
      <ListItemIcon>
        <Today />
      </ListItemIcon>
      <ListItemText primary="Training plan" />
    </ListItem>
  </div>
);

const secondaryListItems = (
  <div>
    <ListSubheader inset>ACCOUNT</ListSubheader>
    <ListItem button component={Link} to="/app/account/profile-information">
      <ListItemIcon>
        <Person />
      </ListItemIcon>
      <ListItemText primary="Profile information" />
    </ListItem>
    <ListItem button component={Link} to="/app/account/settings">
      <ListItemIcon>
        <Settings />
      </ListItemIcon>
      <ListItemText primary="Settings" />
    </ListItem>
  </div>
);

const MenuDrawer = () => {
  const classes = useStyles();
  const { drawerMenuClosed } = useSelector(({ ui }) => ui);

  return (
    <Drawer
      variant="permanent"
      classes={{
        paper: clsx(
          classes.drawerPaper,
          drawerMenuClosed && classes.drawerPaperClose,
        ),
      }}
      open={!drawerMenuClosed}
    >
      <DrawerToolbar />
      <List>{mainListItems}</List>
      <List>{secondaryListItems}</List>
    </Drawer>
  );
};

export default MenuDrawer;
