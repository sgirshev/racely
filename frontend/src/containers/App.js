import React, { useEffect } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import {
  fetchUserData,
  setAuthCredentials,
  setInitUrl,
} from 'store/actions/Auth';
import Layout from 'containers/Layout';
import SignIn from 'containers/SignIn';
import SignUp from 'containers/SignUp';
import { ThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import theme from 'styles/theme';
import PrivateRoute from 'containers/PrivateRoute';
import querystring from 'query-string';

const App = ({ match, location, history }) => {
  const dispatch = useDispatch();
  const { accessToken, initUrl } = useSelector(({ auth }) => auth);

  useEffect(() => {
    if (location.pathname === '/auth' && location.search) {
      const authCredentials = querystring.parse(location.search);
      dispatch(setAuthCredentials(authCredentials));
    }
    return () => window.history.replaceState(null, null, location.pathname);
  }, []);

  useEffect(() => {
    if (accessToken !== null) {
      dispatch(fetchUserData());
    }
  }, [dispatch, accessToken]);

  useEffect(() => {
    if (initUrl === '') {
      dispatch(setInitUrl(location.pathname));
    }
  }, [dispatch, initUrl, history.location.pathname, location.search]);

  if (location.pathname === '/' || location.pathname === '/auth') {
    if (accessToken === null) {
      return <Redirect to={'/signin'} />;
    } else {
      return <Redirect to={'/app/main/dashboard'} />;
    }
  }

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Switch>
        <PrivateRoute
          path={`${match.url}app`}
          accessToken={accessToken}
          component={Layout}
        />
        <Route path="/signin" component={SignIn} />
        <Route path="/signup" component={SignUp} />
      </Switch>
    </ThemeProvider>
  );
};

export default App;
