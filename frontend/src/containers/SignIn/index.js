import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import {
  Button,
  CssBaseline,
  TextField,
  Grid,
  Typography,
  CircularProgress,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import useAuthForm from 'utils/hooks/useAuthForm';
import { useDispatch, useSelector } from 'react-redux';
import { signIn } from 'store/actions/Auth';
import { hideAlert } from 'store/actions/UI';
import Image300 from 'assets/Images/login-page-background-300.jpg';
import theme from 'styles/theme';
import Alert from 'components/Alert';
import Oauth from 'components/Oauth';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  image: {
    backgroundImage: `linear-gradient(to right bottom,
      ${theme.palette.primary.main}F2, 
      ${theme.palette.primary.dark}F2), 
      url(${Image300})`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  authPaper: {
    background: theme.palette.primary.main,
  },
  paper: {
    position: 'relative',
    width: '60%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    margin: theme.spacing(16, 'auto'),
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(4),
  },
  submit: {
    margin: theme.spacing(3, 'auto'),
  },
  oauthContainer: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  authOptions: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  activeAuthMethod: {
    borderBottom: `${theme.palette.primary.main} solid 1px`,
  },
  disabledAuthMethod: {
    color: theme.palette.text.disabled,
  },
  circularProgress: {
    position: 'absolute',
    left: '50%',
    marginLeft: '-20px',
    top: '50%',
    marginTop: '-20px',
  },
  alert: {
    backgroundColor: '#ff0000',
  },
}));

export default function SignIn(props) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { payload, errors, handleChange, handleSubmit } = useAuthForm(() => {
    dispatch(signIn(payload));
  });

  const { accessToken, loading } = useSelector(({ auth }) => auth);
  const { alert } = useSelector(({ ui }) => ui);

  useEffect(() => {
    if (alert.open) {
      setTimeout(() => {
        dispatch(hideAlert());
      }, 5000);
    }
    if (accessToken) {
      props.history.push('/');
    }
  }, [alert, accessToken, props.history, dispatch]);

  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
      <Grid item xs={12} sm={8} md={5} elevation={6}>
        <div className={classes.paper}>
          <div className={classes.authOptions}>
            <Typography
              component="h1"
              variant="h5"
              className={classes.activeAuthMethod}
              style={{ marginRight: '16px' }}
            >
              <Link
                to="/signin"
                style={{
                  color: theme.palette.primary.main,
                  textDecoration: 'none',
                }}
              >
                Sign in
              </Link>
            </Typography>
            <Typography component="h1" variant="h5">
              <Link
                to="/signup"
                style={{
                  color: theme.palette.text.disabled,
                  textDecoration: 'none',
                }}
              >
                Sign up
              </Link>
            </Typography>
          </div>

          <form className={classes.form} noValidate onSubmit={handleSubmit}>
            <TextField
              onChange={handleChange}
              error={!!errors.email}
              helperText={errors.email || ''}
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
            />
            <TextField
              onChange={handleChange}
              error={!!errors.password}
              helperText={errors.password || ''}
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
            />

            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              disabled={loading}
            >
              Sign In
            </Button>
          </form>

          <Oauth
            providers={['google', 'facebook', 'strava']}
            path={'http://localhost:5000/api/auth/authenticate/'}
          />
          {loading && <CircularProgress className={classes.circularProgress} />}
          <Alert open={alert.open} message={alert.message} type={alert.type} />
        </div>
      </Grid>
    </Grid>
  );
}
