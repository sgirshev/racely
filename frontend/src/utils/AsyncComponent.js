import React, { Component } from 'react';

import { CircularProgress } from '@material-ui/core';

const asyncComponent = (importComponent) => {
  // eslint-disable-next-line react/display-name
  return class extends Component {
    state = {
      component: null,
    };

    componentWillUnmount() {
      this.mounted = false;
    }

    async componentDidMount() {
      this.mounted = true;
      const { default: Component } = await importComponent();
      if (this.mounted) {
        this.setState({
          component: <Component {...this.props} />,
        });
      }
    }

    render() {
      return this.state.component || <CircularProgress />;
    }
  };
};

export default asyncComponent;
