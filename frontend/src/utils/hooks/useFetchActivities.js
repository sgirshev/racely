import { useEffect, useState } from 'react';
import api from 'utils/api';

const useFetchActivities = (page, perPage) => {
  const [loading, setLoading] = useState(true);
  const [activities, setActivities] = useState([]);
  const [hasMore, setHasMore] = useState(false);

  useEffect(() => {
    setLoading(true);

    api
      .get(`athlete/activities/`, { params: { page, per_page: perPage } })
      .then((res) => {
        setActivities((prevActivities) => [
          ...prevActivities,
          ...res.data.activities,
        ]);
        setHasMore(res.data.activities.length === perPage);
        setLoading(false);
      });
    return () => {};
  }, [page]);

  return { loading, activities, hasMore };
};

export default useFetchActivities;
