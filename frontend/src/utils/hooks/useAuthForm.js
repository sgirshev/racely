import { useState, useEffect } from 'react';

const useAuthForm = (callback) => {
  const [payload, setPayload] = useState({});
  const [errors, setErrors] = useState({});
  const [isSubmitting, setIsSubmitting] = useState(false);

  useEffect(() => {
    if (Object.keys(errors).length === 0 && isSubmitting) {
      callback();
    }
  }, [errors, callback, isSubmitting]);

  const validate = (payload) => {
    const errors = {};
    if (!payload.email) {
      errors.email = 'Email address is required';
    } else if (!/\S+@\S+\.\S+/.test(payload.email)) {
      errors.email = 'Email address is invalid';
    }

    if (!payload.password) {
      errors.password = 'Password is required';
    } else if (payload.password.length < 6) {
      errors.password = 'Password should be at least 6 characters';
    }
    return errors;
  };

  const handleSubmit = (event) => {
    if (event) event.preventDefault();
    setIsSubmitting(true);
    setErrors(validate(payload));
  };

  const handleChange = (event) => {
    event.persist();
    setPayload((payload) => ({
      ...payload,
      [event.target.name]: event.target.value,
    }));
  };

  return {
    handleChange,
    handleSubmit,
    payload,
    errors,
  };
};

export default useAuthForm;
