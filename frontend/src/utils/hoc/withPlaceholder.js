import React from 'react';
import { Skeleton } from '@material-ui/lab';

export default (Component) => ({ loading, data, ...props }) => {
  if (loading || data === null) return <Skeleton variant="rect" {...props} />;
  return <Component loading={loading} data={data} {...props} />;
};
