import { all } from 'redux-saga/effects';
import auth from 'store/sagas/Auth';
import activities from 'store/sagas/Activities';

export default function* rootSaga(getState) {
  yield all([auth(), activities()]);
}
