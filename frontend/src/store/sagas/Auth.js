import { all, fork, put, takeEvery, delay } from 'redux-saga/effects';
import {
  SIGN_IN,
  SIGN_OUT,
  SIGN_UP,
  FETCH_USER_DATA,
  REFRESH_TOKEN_AFTER_EXPIRATION,
  UPDATE_AUTH_CREDENTIALS,
  SET_AUTH_CREDENTIALS,
} from 'constants/ActionTypes';
import {
  signInSuccess,
  signOutSuccess,
  signUpSuccess,
  fetchUserDataStart,
  fetchUserDataSuccess,
  fetchUserDataFailure,
  signInStart,
  signUpStart,
  signUpFailure,
  refreshTokenAfterExpiration,
  signOut,
  setAuthCredentials,
  setAuthCredentialsSuccess,
  setAuthCredentialsFailure,
  updateAuthCredentials,
} from 'store/actions/Auth';
import { showAlert } from 'store/actions/UI';
import api from 'utils/api';

function* signUpSaga({ payload }) {
  yield put(signUpStart());
  const { firstName, lastName, email, password } = payload;
  try {
    const { data } = yield api.post('auth/signup', {
      firstName,
      lastName,
      email,
      password,
    });
    if (!data.success) {
      yield put(signUpFailure(data.error));
      yield put(showAlert(data.message));
    } else {
      yield put(setAuthCredentials(data.credentials));
      yield put(signUpSuccess(data.credentials.userId));
    }
  } catch (error) {
    yield put(signUpFailure(error));
    yield put(showAlert(error));
  }
}

function* signInSaga({ payload }) {
  yield put(signInStart());
  const { email, password } = payload;
  try {
    const data = yield api.post('auth/signin', {
      email: email,
      password: password,
    });
    if (!data.success) {
      yield put(signUpFailure(data.error));
      yield put(showAlert(data.message));
    } else {
      yield put(setAuthCredentials(data.credentials));
      yield put(
        signInSuccess(data.credentials.userId, data.credentials.accessToken),
      );
    }
  } catch (error) {
    yield put(signUpFailure(error));
    yield put(showAlert({ message: error.message, type: 'error' }));
  }
}

function* setAuthCredentialsSaga({ payload }) {
  const { accessToken, expiresAt, refreshToken } = payload;
  try {
    yield localStorage.setItem('accessToken', accessToken);
    yield localStorage.setItem('expiresAt', expiresAt);
    yield localStorage.setItem('refreshToken', refreshToken);
    yield put(setAuthCredentialsSuccess(payload));
    yield put(refreshTokenAfterExpiration(expiresAt, refreshToken));
  } catch (error) {
    yield put(setAuthCredentialsFailure(new Error('Authentication failure')));
  }
}

function* refreshTokenAfterExpirationSaga({ payload }) {
  yield delay(payload.expiresAt - Date.now());
  try {
    const response = yield api.get('auth/token', {
      params: { refreshToken: payload.refreshToken },
    });
    if (response.data.success) {
      yield put(setAuthCredentials(response.data.credentials));
    }
  } catch (error) {
    yield put(setAuthCredentialsFailure(new Error('Authentication failure')));
    yield put(signOut());
  }
}

function* fetchUserSaga() {
  yield put(fetchUserDataStart());
  try {
    const response = yield api.get('auth/me');
    if (!response.data.success) {
      yield put(fetchUserDataFailure(response.data.error));
      yield put(showAlert(response.data.message));
    } else {
      yield put(fetchUserDataSuccess(response.data.user));
    }
  } catch (error) {
    if (error.response.status === 401) {
      yield put(signOut());
      yield put(
        showAlert('Current session has been expired. Please, sign in.'),
      );
    } else {
      yield put(fetchUserDataFailure(error));
      yield put(showAlert(error.message));
    }
  }
}

function* signOutSaga() {
  yield localStorage.removeItem('userId');
  yield localStorage.removeItem('accessToken');
  yield localStorage.removeItem('expiresAt');
  yield localStorage.removeItem('refreshToken');
  yield put(signOutSuccess());
}

function* watchSignUp() {
  yield takeEvery(SIGN_UP, signUpSaga);
}

function* watchSignIn() {
  yield takeEvery(SIGN_IN, signInSaga);
}

function* watchFetchUser() {
  yield takeEvery(FETCH_USER_DATA, fetchUserSaga);
}

function* watchSignOut() {
  yield takeEvery(SIGN_OUT, signOutSaga);
}

function* watchSetAuthCredentials() {
  yield takeEvery(SET_AUTH_CREDENTIALS, setAuthCredentialsSaga);
}

function* watchRefreshTokenAfterExpiration() {
  yield takeEvery(
    REFRESH_TOKEN_AFTER_EXPIRATION,
    refreshTokenAfterExpirationSaga,
  );
}

export default function* rootSaga() {
  yield all([
    fork(watchSignUp),
    fork(watchSignIn),
    fork(watchFetchUser),
    fork(watchSignOut),
    fork(watchSetAuthCredentials),
    fork(watchRefreshTokenAfterExpiration),
  ]);
}
