import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import { FETCH_ACTIVITIES } from 'constants/ActionTypes';
import {
  fetchActivitiesSuccess,
  fetchActivitiesStart,
  fetchActivitiesFailure,
} from 'store/actions/Activities';
import { showAlert } from 'store/actions/UI';

import api from 'utils/api';

const fetchActivitiesRequest = async (period) =>
  await api
    .get(`athlete/activities/${period}`)
    .then(({ data }) => data)
    .catch((error) => error);

function* fetchActivities({ payload }) {
  yield put(fetchActivitiesStart());
  try {
    const data = yield call(fetchActivitiesRequest, payload);
    if (data.success) {
      yield put(fetchActivitiesSuccess(data.activities));
    } else {
      yield put(showAlert(data.message));
    }
  } catch (error) {
    yield put(fetchActivitiesFailure(error));
    yield put(showAlert(error.message));
  }
}

function* watchFetchActivities() {
  yield takeEvery(FETCH_ACTIVITIES, fetchActivities);
}

export default function* rootSaga() {
  yield all([fork(watchFetchActivities)]);
}
