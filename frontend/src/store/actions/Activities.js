import {
  FETCH_ACTIVITIES,
  FETCH_ACTIVITIES_START,
  FETCH_ACTIVITIES_SUCCESS,
  FETCH_ACTIVITIES_FAILURE,
} from 'constants/ActionTypes';

export const fetchActivities = (period) => ({
  type: FETCH_ACTIVITIES,
  payload: period,
});

export const fetchActivitiesStart = () => ({
  type: FETCH_ACTIVITIES_START,
});

export const fetchActivitiesSuccess = (activities) => ({
  type: FETCH_ACTIVITIES_SUCCESS,
  payload: activities,
});

export const fetchActivitiesFailure = (error) => ({
  type: FETCH_ACTIVITIES_FAILURE,
  payload: error,
});
