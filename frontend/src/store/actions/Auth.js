import {
  FETCH_USER_DATA,
  FETCH_USER_DATA_START,
  FETCH_USER_DATA_SUCCESS,
  FETCH_USER_DATA_FAILURE,
  INIT_URL,
  SIGN_IN,
  SIGN_IN_START,
  SIGN_IN_SUCCESS,
  SIGN_IN_FAILURE,
  SIGN_OUT,
  SIGN_OUT_SUCCESS,
  SIGN_UP,
  SIGN_UP_START,
  SIGN_UP_SUCCESS,
  SIGN_UP_FAILURE,
  SET_AUTH_CREDENTIALS,
  SET_AUTH_CREDENTIALS_SUCCESS,
  SET_AUTH_CREDENTIALS_FAILURE,
  REFRESH_TOKEN_AFTER_EXPIRATION,
  UPDATE_AUTH_CREDENTIALS,
  SET_AUTH_CONFLICT,
  CLEAR_ACCOUNT_CONFLICT,
  SET_MERGE_TOKEN,
  SET_USER,
} from 'constants/ActionTypes';

export const setInitUrl = (url) => {
  return {
    type: INIT_URL,
    payload: url,
  };
};

export const signUp = (payload) => ({
  type: SIGN_UP,
  payload,
});

export const signUpStart = () => ({
  type: SIGN_UP_START,
});

export const signUpSuccess = (userId) => ({
  type: SIGN_UP_SUCCESS,
  payload: userId,
});
export const signUpFailure = (error) => ({
  type: SIGN_UP_FAILURE,
  payload: error,
});

export const signIn = (payload) => ({
  type: SIGN_IN,
  payload,
});

export const signInStart = () => ({
  type: SIGN_IN_START,
});

export const signInSuccess = (userId) => ({
  type: SIGN_IN_SUCCESS,
  payload: userId,
});

export const signInFailure = (error) => ({
  type: SIGN_IN_FAILURE,
  payload: error,
});

export const signOut = () => ({
  type: SIGN_OUT,
});

export const signOutSuccess = () => ({
  type: SIGN_OUT_SUCCESS,
});

export const fetchUserData = () => ({
  type: FETCH_USER_DATA,
});

export const fetchUserDataStart = () => ({
  type: FETCH_USER_DATA_START,
});

export const fetchUserDataSuccess = (userData) => ({
  type: FETCH_USER_DATA_SUCCESS,
  payload: userData,
});

export const fetchUserDataFailure = (error) => ({
  type: FETCH_USER_DATA_FAILURE,
  payload: error,
});

export const setAuthCredentials = (authCredentials) => ({
  type: SET_AUTH_CREDENTIALS,
  payload: authCredentials,
});

export const setAuthCredentialsSuccess = (userId) => ({
  type: SET_AUTH_CREDENTIALS_SUCCESS,
  payload: userId,
});

export const setAuthCredentialsFailure = (error) => ({
  type: SET_AUTH_CREDENTIALS_FAILURE,
  payload: error,
});

export const updateAuthCredentials = (
  accessToken,
  expiresAt,
  refreshToken,
) => ({
  type: UPDATE_AUTH_CREDENTIALS,
  payload: { accessToken, expiresAt, refreshToken },
});

export const refreshTokenAfterExpiration = (expiresAt, refreshToken) => ({
  type: REFRESH_TOKEN_AFTER_EXPIRATION,
  payload: { expiresAt, refreshToken },
});

export const setAuthConflict = (authConflictData) => ({
  type: SET_AUTH_CONFLICT,
  payload: authConflictData,
});

export const clearAccountConflict = () => ({
  type: CLEAR_ACCOUNT_CONFLICT,
});

export const setMergeToken = (mergeToken) => ({
  type: SET_MERGE_TOKEN,
  payload: mergeToken,
});

export const setUserData = (userData) => ({
  type: SET_USER,
  payload: userData,
});
