import { HIDE_ALERT, SHOW_ALERT, TOGGLE_MENU } from 'constants/ActionTypes';

export const toggleMenu = (isMenuClosed) => ({
  type: TOGGLE_MENU,
  payload: isMenuClosed,
});

export const showAlert = ({ message, type }) => {
  return {
    type: SHOW_ALERT,
    payload: { message, type },
  };
};

export const hideAlert = () => ({
  type: HIDE_ALERT,
});
