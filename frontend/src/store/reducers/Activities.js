import {
  FETCH_ACTIVITIES_FAILURE,
  FETCH_ACTIVITIES_START,
  FETCH_ACTIVITIES_SUCCESS,
} from 'constants/ActionTypes';

const INIT_STATE = {
  activities: [],
  loading: false,
  error: null,
};

const Activities = (state = INIT_STATE, action) => {
  switch (action.type) {
    case FETCH_ACTIVITIES_START:
      return {
        ...state,
        loading: true,
      };
    case FETCH_ACTIVITIES_SUCCESS:
      return {
        ...state,
        activities: action.payload,
        loading: false,
      };
    case FETCH_ACTIVITIES_FAILURE:
      return {
        ...state,
        error: action.payload,
        loading: false,
      };

    default:
      return state;
  }
};

export default Activities;
