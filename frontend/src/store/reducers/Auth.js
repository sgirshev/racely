import {
  INIT_URL,
  FETCH_USER_DATA_SUCCESS,
  SIGN_IN_SUCCESS,
  SIGN_OUT_SUCCESS,
  SIGN_UP_SUCCESS,
  SIGN_UP_FAILURE,
  SIGN_UP_START,
  SIGN_IN_START,
  SIGN_IN_FAILURE,
  FETCH_USER_DATA_START,
  FETCH_USER_DATA_FAILURE,
  SET_AUTH_CREDENTIALS_SUCCESS,
  SET_AUTH_CREDENTIALS_FAILURE,
  SET_AUTH_CONFLICT,
  CLEAR_ACCOUNT_CONFLICT,
  SET_MERGE_TOKEN,
  SET_USER,
} from 'constants/ActionTypes';

const INIT_STATE = {
  accessToken: localStorage.getItem('accessToken') || null,
  user: localStorage.getItem('user') || null,
  loading: false,
  initUrl: localStorage.getItem('initUrl') || '',
  error: null,
  authConflict: JSON.parse(localStorage.getItem('authConflict')) || null,
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case SIGN_UP_START: {
      return {
        ...state,
        loading: true,
      };
    }
    case SIGN_UP_SUCCESS: {
      return {
        ...state,
        loading: false,
        accessToken: action.payload,
      };
    }
    case SIGN_UP_FAILURE: {
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    }
    case SIGN_IN_START: {
      return {
        ...state,
        loading: false,
      };
    }
    case SIGN_IN_SUCCESS: {
      return {
        ...state,
        loading: false,
        accessToken: action.payload,
      };
    }
    case SIGN_IN_FAILURE: {
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    }
    case FETCH_USER_DATA_START: {
      return {
        ...state,
        loading: true,
      };
    }
    case FETCH_USER_DATA_SUCCESS: {
      return {
        ...state,
        loading: false,
        user: action.payload,
      };
    }
    case FETCH_USER_DATA_FAILURE: {
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    }
    case SIGN_OUT_SUCCESS: {
      return {
        ...state,
        accessToken: null,
        initURL: '/app/dashboard',
        user: null,
        loading: false,
      };
    }
    case INIT_URL: {
      return {
        ...state,
        initURL: action.payload,
      };
    }
    case SET_AUTH_CREDENTIALS_SUCCESS: {
      return {
        ...state,
        accessToken: action.payload.accessToken,
      };
    }

    case SET_AUTH_CREDENTIALS_FAILURE: {
      return {
        ...state,
        error: action.payload,
      };
    }

    case SET_AUTH_CONFLICT: {
      return {
        ...state,
        authConflict: action.payload,
      };
    }

    case CLEAR_ACCOUNT_CONFLICT: {
      return {
        ...state,
        authConflict: null,
      };
    }

    case SET_MERGE_TOKEN: {
      return {
        ...state,
        authConflict: {
          ...state.authConflict,
          mergeToken: action.payload,
        },
      };
    }

    case SET_USER: {
      return {
        ...state,
        user: action.payload,
      };
    }

    default:
      return state;
  }
};
