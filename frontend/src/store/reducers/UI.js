import { HIDE_ALERT, SHOW_ALERT, TOGGLE_MENU } from 'constants/ActionTypes';
const INIT_STATE = {
  drawerMenuClosed: true,
  alert: {
    open: false,
    message: '',
    type: '',
  },
};

const UI = (state = INIT_STATE, action) => {
  switch (action.type) {
    case TOGGLE_MENU:
      return {
        ...state,
        drawerMenuClosed: action.payload,
      };
    default:
      return state;

    case SHOW_ALERT: {
      return {
        ...state,
        alert: {
          ...state.alert,
          open: true,
          message: action.payload.message,
          type: action.payload.type,
        },
      };
    }

    case HIDE_ALERT: {
      return {
        ...state,
        alert: {
          ...state.alert,
          open: false,
          message: '',
          type: '',
        },
      };
    }
  }
};

export default UI;
