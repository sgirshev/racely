import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import Auth from 'store/reducers/Auth';
import UI from 'store/reducers/UI';
import Activities from 'store/reducers/Activities';

const createRootReducer = (history) =>
  combineReducers({
    router: connectRouter(history),
    auth: Auth,
    ui: UI,
    activities: Activities,
  });

export default createRootReducer;
