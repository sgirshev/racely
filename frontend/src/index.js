import React from 'react';
import { render } from 'react-dom';
import AppRouter from './AppRouter';

const renderApp = () => render(<AppRouter />, document.getElementById('root'));

renderApp();
