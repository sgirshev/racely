// Auth
export const SIGN_UP = 'sign_up';
export const SIGN_UP_START = 'sign_up_start';
export const SIGN_UP_SUCCESS = 'sign_up_success';
export const SIGN_UP_FAILURE = 'sign_up_failure';

export const SIGN_IN = 'sign_in';
export const SIGN_IN_START = 'sign_in_start';
export const SIGN_IN_SUCCESS = 'sign_in_success';
export const SIGN_IN_FAILURE = 'sign_in_failure';

export const SIGN_OUT = 'sign_out';
export const SIGN_OUT_SUCCESS = 'sign_out_success';

export const SET_USER = 'set_user';

export const INIT_URL = 'init_url';

export const SET_AUTH_CONFLICT = 'set_auth_conflict';
export const CLEAR_ACCOUNT_CONFLICT = 'clear_account_conflict';
export const SET_MERGE_TOKEN = 'set_merge_token';

export const SET_AUTH_CREDENTIALS = 'set_aut_credentials';
export const SET_AUTH_CREDENTIALS_SUCCESS = 'set_aut_credentials_success';
export const SET_AUTH_CREDENTIALS_FAILURE = 'set_aut_credentials_failure';

export const REFRESH_TOKEN_AFTER_EXPIRATION = 'refresh_token_after_expiration';
export const UPDATE_AUTH_CREDENTIALS = 'update_auth_credentials';

// User
export const FETCH_USER_DATA = 'fetch_user_data';
export const FETCH_USER_DATA_START = 'fetch_user_data_start';
export const FETCH_USER_DATA_SUCCESS = 'fetch_user_data_success';
export const FETCH_USER_DATA_FAILURE = 'fetch_user_data_failure';

// UI
export const SHOW_ALERT = 'show_alert';
export const HIDE_ALERT = 'hide_alert';
export const TOGGLE_MENU = 'toggle_menu';

// Activities
export const FETCH_ACTIVITIES = 'fetch_activities';
export const FETCH_ACTIVITIES_START = 'fetch_activities_start';
export const FETCH_ACTIVITIES_SUCCESS = 'fetch_activities_success';
export const FETCH_ACTIVITIES_FAILURE = 'fetch_activities_failure';
