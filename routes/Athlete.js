const router = require('express').Router();
const passport = require('passport');
const { User, AccessToken, RefreshToken } = require('../models/Users');
const strava = require('../utils/strava');
const redis = require('../utils/redis');

const getStravaAccessToken = async (userId) => {
  try {
    const { accessToken, expiresAt } = await AccessToken.findOne({
      userId: userId,
      apiType: 'strava',
    });
    console.log(accessToken, expiresAt);
    if (!accessToken) return null;
    if (expiresAt < Date.now()) {
      const { refreshToken } = await RefreshToken.findOne({ userId });
      const credentials = await strava.updateCredentials(refreshToken);
      const { accessToken } = await AccessToken.findOneAndUpdate(
        { userId: userId, apiType: 'strava' },
        {
          $set: {
            accessToken: credentials.access_token,
            expiresAt: credentials.expires_at,
            apiType: 'strava',
          },
        },
        { new: true, upsert: true },
      );
      await RefreshToken.findOneAndUpdate(
        { userId: userId, apiType: 'strava' },
        { refreshToken: credentials.refresh_token, apiType: 'strava' },
        { new: true, upsert: true },
      );
      return accessToken;
    }
    return accessToken;
  } catch (error) {
    console.log('Error getting access token', error.message);
  }
};

const checkCache = (req, res, next) => {
  const { id } = req.user;
  const period = req.params.period;
  const key = `${id}_${period}`;
  redis.get(key, (err, data) => {
    if (err) {
      console.log('[Redis] Error reading cache data', err);
      next();
    }
    if (data) {
      return res.json({
        success: true,
        activities: JSON.parse(data),
        cache: true,
      });
    } else {
      next();
    }
  });
};

const getTimeStamp = (moment) => {
  const now = new Date();
  let timestamp;

  switch (moment) {
    case 'year':
      return (timestamp = Math.floor(new Date(now.getFullYear(), 0) / 1000));
    case 'month':
      return (timestamp = Math.floor(
        new Date(now.getFullYear(), now.getMonth()) / 1000,
      ));
    case 'week':
      return (timestamp =
        Math.floor(
          new Date(now.getFullYear(), now.getMonth(), now.getDate()) / 1000,
        ) -
        (now.getDay() - 1) * 86400);
    default:
      return -1;
  }
};

router.get(
  '/activities',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const { id } = req.user;
    const params = req.query;
    try {
      const accessToken = await getStravaAccessToken(id);
      if (!accessToken) {
        res.status(401).json({
          success: false,
          message:
            'User need to be authorized with Strava to fetch related data',
        });
      }
      const activities = await strava.athlete.activities.get(
        accessToken,
        params,
      );
      return res.json({ success: true, activities });
    } catch (error) {
      console.log(
        '[Athlete route] -- Error getting /activities -- ',
        error.message,
      );
    }
  },
);

router.get(
  '/activities/:period',
  passport.authenticate('jwt', { session: false }),
  checkCache,
  async (req, res) => {
    const { id } = req.user;
    const period = req.params.period;
    const after = getTimeStamp(period);

    try {
      const accessToken = await getStravaAccessToken(id);
      if (!accessToken) {
        res.status(401).json({
          success: false,
          message:
            'User need to be authorized with Strava to fetch related data',
        });
      }
      const activities = await strava.athlete.activities.listByPeriod(
        accessToken,
        {
          after,
        },
      );
      redis.setex(`${id}_${period}`, 3600, JSON.stringify(activities));

      return res.json({ success: true, activities, cache: false });
    } catch (error) {
      console.log(
        '[Athlete route] -- Error getting /activities/:moment -- ',
        error.message,
      );
    }
  },
);

router.get('/:id', async (req, res) => {
  const athleteId = req.params.id;
  try {
    const user = await User.findOne({ _id: athleteId });
    if (user) {
      const {
        firstName,
        lastName,
        profileImageUrl,
        emails,
        created,
        _id,
      } = user;
      res.json({
        success: true,
        userData: {
          firstName,
          lastName,
          profileImageUrl,
          email: emails[0],
          created,
          id: _id,
        },
      });
    } else {
      res.sendStatus(404);
    }
  } catch (error) {
    console.log(error);
  }
});

module.exports = router;
