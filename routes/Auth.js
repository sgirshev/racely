const router = require('express').Router();
const bcrypt = require('bcryptjs');
const passport = require('passport');
const util = require('util');
const validateAuthInput = require('../validation/auth');
const auth = require('../utils/auth');
const { User } = require('../models/Users');
const { url, oauthStrategiesConfig } = require('../config/default');

const generateSalt = util.promisify(bcrypt.genSalt);
const generateHash = util.promisify(bcrypt.hash);
const comparePasswords = util.promisify(bcrypt.compare);

/**
 *  User authentication with login / password
 *  ============================================================================
 */

router.post('/signup', async (req, res) => {
  const { errors, isValid } = validateAuthInput(req.body);
  if (!isValid) return res.status(400).json(errors);
  const user = await User.findOne({ email: req.body.email });
  if (user)
    return res
      .status(400)
      .json({ success: false, message: 'Email already exists' });

  const { email, password } = req.body;

  const newUser = new User({ email });

  try {
    const salt = await generateSalt(10);
    newUser.hashedPassword = await generateHash(password, salt);
    const { id } = await newUser.save();
    const credentials = await auth.getOrUpdateAuthCredentials(id, 'local');
    res.json({
      success: true,
      credentials,
    });
  } catch (error) {
    console.log(error);
    res.sendStatus(500);
  }
});

router.post('/signin', async (req, res) => {
  const { errors, isValid } = validateAuthInput(req.body);
  if (!isValid) return res.status(400).json(errors);
  const user = await User.findOne({ email: req.body.email });
  if (!user)
    return res.json({ success: false, message: 'User does not exist' });

  const { password } = req.body;

  try {
    if (!(await comparePasswords(password, user.hashedPassword))) {
      return res.json({ success: false, message: 'Invalid credentials' });
    }
    const credentials = await auth.getOrUpdateAuthCredentials(user.id, 'local');
    res.json({
      success: true,
      credentials,
    });
  } catch (error) {
    console.log(error);
    res.sendStatus(500);
  }
});

/**
 *  User authentication using oauth2 providers
 *  ============================================================================
 */

router.get(
  '/authenticate/:provider/callback',
  async (req, res, next) => {
    const provider = req.params.provider;

    passport.authenticate(`${provider}Authentication`, {
      session: false,
      failureRedirect: '/failure',
      callbackURL: `${url}/auth/authenticate/${provider}/callback`,
    })(req, res, next);
  },
  async (req, res) => {
    try {
      const authCredentials = await auth.getOrUpdateAuthCredentials(
        req.user.id,
        'local',
      );
      res.redirect(
        `http://localhost:3000/auth?accessToken=${authCredentials.accessToken}&expiresAt=${authCredentials.expiresAt}&refreshToken=${authCredentials.refreshToken}`,
      );
    } catch (error) {
      console.log('error', error.message);
    }
  },
);

router.get('/authenticate/:provider', (req, res) => {
  const provider = req.params.provider;
  passport.authenticate(`${provider}Authentication`, {
    scope: oauthStrategiesConfig[provider].scope,
    callbackURL: `${url}/auth/authenticate/${provider}/callback`,
  })(req, res);
});

/**
 *  Connect strava account to existing signed-in user
 *  ============================================================================
 */

router.get(
  '/connect/strava/callback',
  async (req, res, next) => {
    passport.authenticate('connectStrava', {
      session: false,
      failureRedirect: '/failure',
      callbackURL: `${url}/auth/connect/strava/callback`,
    })(req, res, next);
  },
  async (req, res) => {
    if (req.user.existedUser) {
      res.redirect(
        `http://localhost:3000/app/extra/auth-conflict?current_user=${req.user.currentUser.id}&existed_user=${req.user.existedUser.id}&methods=${req.user.existedUser.method}`,
      );
    } else if (req.user.currentUserId) {
      res.redirect(
        `http://localhost:3000/app/account/profile-information?userId=${req.user.currentUserId}`,
      );
    } else {
      res.redirect(
        `http://localhost:3000/app/account/profile-information?stravaConnectSuccess=false`,
      );
    }
  },
);

router.get('/connect/strava/:id', (req, res) => {
  const currentUserId = req.params.id;
  passport.authenticate('connectStrava', {
    scope: ['read_all,activity:read_all,profile:read_all'],
    state: currentUserId,
    callbackURL: `${url}/auth/connect/strava/callback`,
  })(req, res);
});

/**
 *
 *  ============================================================================
 */

router.get(
  '/me',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const {
      id,
      firstName,
      lastName,
      emails,
      socialIDs,
      created,
      profileImageUrl,
    } = req.user;

    const stravaId = socialIDs.find((id) => id.type === 'strava');

    const user = {
      id,
      firstName,
      lastName,
      email: emails[0],
      stravaId: stravaId ? stravaId : null,
      created,
      profileImageUrl,
    };

    const responseData = {
      success: true,
      user,
    };
    res.send(responseData);
  },
);

router.get('/token', async (req, res) => {
  const { refreshToken } = req.query;
  if (refreshToken) {
    try {
      const userId = await auth.validateRefreshToken(refreshToken, 'local');
      const credentials = await auth.getOrUpdateAuthCredentials(
        userId,
        'local',
      );
      res.json({
        success: true,
        credentials,
      });
    } catch (error) {
      console.log(error);
      res.status(401).send();
    }
  } else {
    res.status(401).send();
  }
});

router.get('/authorize/:provider/callback', async (req, res, next) => {
  const provider = req.params.provider;
  passport.authenticate(
    `${provider}Authorization`,
    {
      scope: oauthStrategiesConfig[provider].scope,
      callbackURL: `${url}/auth/authorize/${provider}/callback`,
    },
    async function (error, user, message) {
      if (error) {
        return next(error);
      }
      if (!user) {
        return res.redirect(
          `http://localhost:3000/app/extra/merge-accounts?merge_token&message=${message}`,
        );
      }

      try {
        const credentials = await auth.getOrUpdateAuthCredentials(
          user.id,
          'local',
        );
        res.redirect(
          `http://localhost:3000/app/extra/merge-accounts?merge_token=${credentials.accessToken}&message=Successfully authorized`,
        );
      } catch (error) {
        res.redirect(
          `http://localhost:3000/app/extra/merge-accounts?merge_token&message=${error.message}`,
        );
      }
    },
  )(req, res, next);
});

router.get('/authorize/:userId/:provider', async (req, res) => {
  const provider = req.params.provider;
  const userId = req.params.userId;
  passport.authenticate(`${provider}Authorization`, {
    scope: oauthStrategiesConfig[provider].scope,
    state: userId,
    callbackURL: `${url}/auth/authorize/${provider}/callback`,
  })(req, res);
});

router.post(
  '/merge',
  async (req, res, next) => {
    passport.authenticate('jwt', { session: false })(req, res, next);
  },
  async (req, res, next) => {
    const currentUserId = req.user.id;
    const { existedUserToken, mainAccountId } = req.body;

    try {
      const { userId: existedUserId } = await auth.extractFromJWT(
        existedUserToken,
      );

      const user = await auth.mergeAccounts(mainAccountId, [
        currentUserId,
        existedUserId,
      ]);

      const credentials = await auth.getOrUpdateAuthCredentials(
        user.id,
        'local',
      );

      res.json({ success: true, user, credentials });
    } catch (error) {
      next(error);
    }
  },
);

module.exports = router;
