const router = require('express').Router();
const passport = require('passport');
const strava = require('../utils/strava');
const redis = require('../utils/redis');
const auth = require('../utils/auth');

const checkCache = async (key) => {
  return new Promise((resolve, reject) => {
    redis.get(key, (error, data) => {
      if (error) {
        reject(error);
      }
      if (data) {
        resolve(data);
      } else {
        resolve(null);
      }
    });
  });
};

router.get(
  '/:activityId/streams',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const { id: userId } = req.user;
    const activityId = req.params.activityId;
    const queryParams = req.query;

    const cacheKey = `activity_${activityId}_${queryParams.keys}_stream`;

    try {
      const cacheData = await checkCache(cacheKey);
      if (cacheData) {
        return res.json({
          success: true,
          stream: JSON.parse(cacheData),
          cache: true,
        });
      }
    } catch (error) {
      console.log('[Redis] Error reading cache data', error);
    }

    try {
      const { accessToken } = await auth.getOrUpdateAuthCredentials(
        userId,
        'strava',
      );
      const stream = await strava.activity.getStream(
        accessToken,
        activityId,
        queryParams,
      );
      redis.setex(cacheKey, 3600, JSON.stringify(stream));

      return res.json({ success: true, stream, cache: false });
    } catch (error) {
      console.log(
        '[Activities GET streams route] -- Error getting streams by activity ID -- ',
        error.message,
      );
    }
  },
);

router.get(
  '/:activityId',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const { id: userId } = req.user;
    const activityId = req.params.activityId;

    const cacheKey = `activity_${activityId}`;

    try {
      const cacheData = await checkCache(cacheKey);
      if (cacheData) {
        return res.json({
          success: true,
          activity: JSON.parse(cacheData),
          cache: true,
        });
      }
    } catch (error) {
      console.log('[Redis] Error reading cache data', error);
    }

    try {
      const { accessToken } = await auth.getOrUpdateAuthCredentials(
        userId,
        'strava',
      );
      const activity = await strava.activity.get(accessToken, activityId);
      redis.setex(cacheKey, 3600, JSON.stringify(activity));

      return res.json({ success: true, activity, cache: false });
    } catch (error) {
      console.log(
        '[Activities GET activity route] -- Error getting activity by ID -- ',
        error.message,
      );
    }
  },
);

module.exports = router;
