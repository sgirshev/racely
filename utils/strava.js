const {
  clientID,
  clientSecret,
} = require('../config/default').oauthStrategiesConfig.strava.config;
const axios = require('axios');
const qs = require('querystring');

const Strava = function () {
  this.config = {
    auth_url: 'https://www.strava.com/api/v3/oauth/token',
    base_url: 'https://www.strava.com/api/v3',
    client_id: clientID,
    client_secret: clientSecret,
  };

  const self = this;
  this.athlete = {
    activities: {
      get: async function (accessToken, params) {
        return await self._getPage('/athlete/activities', accessToken, params);
      },

      listByPeriod: async function (accessToken, params) {
        return await self._getPages('/athlete/activities', accessToken, params);
      },
    },
  };

  this.activity = {
    get: async function (accessToken, pathParam) {
      return await self._get(`/activities/${pathParam}`, accessToken);
    },
    getStream: async function (accessToken, pathParam, queryPrams) {
      return await self._get(
        `/activities/${pathParam}/streams`,
        accessToken,
        queryPrams,
      );
    },
  };

  this.updateCredentials = async function (refresh_token) {
    const response = await axios.post(
      this.config.auth_url,
      qs.stringify({
        client_id: this.config.client_id,
        client_secret: this.config.client_secret,
        grant_type: 'refresh_token',
        refresh_token,
      }),
    );
    response.data.expires_at *= 1000;
    const {
      access_token: accessToken,
      expires_at: expiresAt,
      refresh_token: refreshToken,
    } = response.data;
    return { accessToken, expiresAt, refreshToken };
  };

  this._get = async function (endpoint, accessToken, queryParams) {
    if (!endpoint) {
      throw new Error('call is required');
    }
    if (!accessToken) {
      throw new Error('Valid access token is required');
    }

    const url = this.config.base_url + endpoint;

    const response = await axios.get(url, {
      headers: { Authorization: 'Bearer ' + accessToken },
      params: queryParams,
    });
    return response.data;
  };

  this._getPage = async function (endpoint, accessToken, params) {
    if (!endpoint) {
      throw new Error('call is required');
    }
    if (!accessToken) {
      throw new Error('Valid access token is required');
    }

    const url = this.config.base_url + endpoint;

    const response = await axios.get(url, {
      headers: { Authorization: 'Bearer ' + accessToken },
      params,
    });
    return response.data;
  };

  this._getPages = async function (endpoint, accessToken, params) {
    const per_page = 200;
    let page = 1;
    const data = [];
    let pageData = [];

    do {
      pageData = await self._getPage(endpoint, accessToken, {
        ...params,
        page,
        per_page,
      });
      pageData.forEach((item) => data.push(item));
      page++;
    } while (pageData.length === per_page);

    return data;
  };
};

module.exports = new Strava();
