const { AccessToken, RefreshToken, User } = require('../models/Users');
const util = require('util');
const strava = require('../utils/strava');
const jwt = require('jsonwebtoken');
const {
  accessTokenExpirationTime,
  refreshTokenExpirationTime,
} = require('../config/default').jwtAuth;
const privateKey = process.env.JWT_PRIVATE_KEY;

const generateToken = util.promisify(jwt.sign);
const verifyToken = util.promisify(jwt.verify);

const auth = {};

auth.validateRefreshToken = async (refreshToken, apiType) => {
  if (!refreshToken || typeof apiType !== 'string') {
    return null;
  }
  const { userId } = await RefreshToken.findOne({
    refreshToken,
    apiType,
  });
  return userId ? userId : null;
};

auth.extractFromJWT = async (accessToken) => {
  const decoded = await verifyToken(accessToken, process.env.JWT_PRIVATE_KEY);
  return decoded;
};

auth.saveAuthCredentials = async (
  userId,
  accessToken,
  expiresAt,
  refreshToken,
  apiType,
) => {
  try {
    await AccessToken.findOneAndUpdate(
      { userId: userId, apiType: apiType },
      {
        $set: {
          accessToken: accessToken,
          expiresAt: expiresAt,
        },
      },
      { new: true, upsert: true },
    );
    await RefreshToken.findOneAndUpdate(
      { userId: userId, apiType: apiType },
      { refreshToken: refreshToken },
      { new: true, upsert: true },
    );
  } catch (error) {
    throw new Error(error);
  }
};

auth._generateToken = async (userId) => {
  const accessToken = await generateToken({ userId }, privateKey, {
    expiresIn: accessTokenExpirationTime,
  });
  const expiresAt = Date.now() + accessTokenExpirationTime * 1000;
  const refreshToken = await generateToken({ userId }, privateKey, {
    expiresIn: refreshTokenExpirationTime,
  });

  return { accessToken, expiresAt, refreshToken };
};

auth.getOrUpdateAuthCredentials = async (userId, apiType) => {
  if (!userId || typeof apiType !== 'string') {
    throw new Error('Server error');
  }

  let accessToken, expiresAt, refreshToken;

  const existedAccessToken = await AccessToken.findOne({
    userId: userId,
    apiType: apiType,
  });

  const existedRefreshToken = await RefreshToken.findOne({
    userId: userId,
    apiType: apiType,
  });

  if (existedAccessToken && existedRefreshToken) {
    accessToken = existedAccessToken.accessToken;
    expiresAt = existedAccessToken.expiresAt;
    refreshToken = existedRefreshToken.refreshToken;
  } else {
    const token = await auth._generateToken(userId);
    accessToken = token.accessToken;
    expiresAt = token.expiresAt;
    refreshToken = token.refreshToken;
  }

  if (expiresAt < Date.now()) {
    if (apiType === 'local') {
      const token = await auth._generateToken(userId);
      accessToken = token.accessToken;
      expiresAt = token.expiresAt;
      refreshToken = token.refreshToken;
    } else if (apiType === 'strava') {
      const credentials = await strava.updateCredentials(refreshToken);
      accessToken = credentials.accessToken;
      expiresAt = credentials.expiresAt;
      refreshToken = credentials.refreshToken;
    } else {
      return null;
    }
  }

  try {
    await AccessToken.findOneAndUpdate(
      { userId: userId, apiType: apiType },
      {
        $set: {
          accessToken,
          expiresAt,
          apiType,
        },
      },
      { new: true, upsert: true },
    );
    await RefreshToken.findOneAndUpdate(
      { userId: userId, apiType: apiType },
      {
        refreshToken,
        expiresAt,
        apiType,
      },
      { new: true, upsert: true },
    );
  } catch (error) {
    console.log(error);
    throw new Error(
      `[AUTH] Error saving new auth credentials to DB: ${error.message}`,
    );
  }

  return {
    accessToken,
    expiresAt,
    refreshToken,
  };
};

auth.mergeAccounts = async (mainAccountId, userIds) => {
  if (
    typeof mainAccountId !== 'string' ||
    !(userIds instanceof Array) ||
    userIds.length < 2
  )
    throw new Error('[Merge accounts]: Invalid options');

  const accounts = await User.find({ _id: { $in: userIds } });
  const mainAccountIndex = accounts.findIndex(
    (account) => account.id === mainAccountId,
  );

  const mainAccount = accounts[mainAccountIndex];
  const deleteIds = [];

  accounts.forEach((account, index) => {
    if (index !== mainAccountIndex) {
      mainAccount.emails.push(...account.emails);
      mainAccount.socialIDs.push(...account.socialIDs);
      deleteIds.push(account.id);
    }
  });

  await mainAccount.save();

  await User.deleteMany({ _id: { $in: deleteIds } });
  await AccessToken.deleteMany({ userId: { $in: deleteIds } });
  await RefreshToken.deleteMany({ userId: { $in: deleteIds } });

  return mainAccount;
};

module.exports = auth;
