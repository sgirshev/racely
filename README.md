# **RACELY**

Application for tracking running, cycling, and swimming activities. It synchronizes with your Strava account to visualize data. 
It will be possible in the future to create a personalized training plan and get data from health tracking services such as Google Health.
