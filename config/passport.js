const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const FacebookStrategy = require('passport-facebook').Strategy;
const StravaStrategy = require('passport-strava-oauth2').Strategy;
const { url, oauthStrategiesConfig } = require('../config/default');
const { User } = require('../models/Users');
const auth = require('../utils/auth');

module.exports = (passport) => {
  passport.serializeUser((user, done) => {
    done(null, user._id);
  });
  passport.deserializeUser((id, done) => {
    User.findById(id)
      .then((user) => done(null, user))
      .catch((err) => done(err));
  });

  const options = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: process.env.JWT_PRIVATE_KEY,
  };

  const authenticationCallback = async (
    req,
    accessToken,
    refreshToken,
    params,
    profile,
    done,
  ) => {
    let user;

    try {
      user = await User.findOne({
        socialIDs: {
          $elemMatch: {
            id: profile.id,
            type: profile.provider,
          },
        },
      });
      if (!user) {
        user = await new User({
          firstName: profile.name.givenName,
          lastName: profile.name.familyName,
          profileImageUrl: profile.photos[0].value,
          emails: [profile.emails[0].value],
          socialIDs: [{ id: profile.id, type: profile.provider }],
        }).save();
      }

      if (profile.provider === 'strava') {
        await auth.saveAuthCredentials(
          user._id,
          accessToken,
          params.expires_at * 1000,
          refreshToken,
          'strava',
        );
      }
    } catch (error) {
      console.log(error);
      done(error, null);
    }
    done(null, user);
  };

  const connectStravaCallback = async (
    req,
    accessToken,
    refreshToken,
    params,
    profile,
    done,
  ) => {
    if (!req.query.state)
      done(
        new Error(
          '[Connect Strava strategy] Authentication failed. Current User ID is required',
        ),
        null,
      );
    const currentUserId = req.query.state;

    try {
      const existedUser = await User.findOne({
        socialIDs: {
          $elemMatch: {
            id: profile.id,
            type: 'strava',
          },
        },
      });

      if (existedUser) {
        if (existedUser.id === currentUserId)
          return done(null, { currentUserId });
        const { socialIDs } = existedUser;
        const authMethods = socialIDs.reduce(
          (methods, currentMethod, index) => {
            if (currentMethod.type !== 'strava') {
              methods += currentMethod.type;
              index < socialIDs.length - 1 && (methods += ',');
            }
            return methods;
          },
          '',
        );
        return done(null, {
          currentUser: { id: currentUserId },
          existedUser: {
            id: existedUser._id,
            method: authMethods.length !== 0 ? authMethods : 'unknown',
          },
        });
      }

      let user = await User.findOne({
        _id: currentUserId,
      });
      user.socialIDs.push({ id: profile.id, type: profile.provider });
      user = await user.save();

      await auth.saveAuthCredentials(
        user._id,
        accessToken,
        params.expires_at * 1000,
        refreshToken,
        'strava',
      );
      return done(null, { currentUserId });
    } catch (error) {
      console.log(error);
      done(error, null);
    }
  };

  const authorizationCallback = async (
    req,
    accessToken,
    refreshToken,
    params,
    profile,
    done,
  ) => {
    const userId = req.query.state;
    let user;
    try {
      user = await User.findOne({
        socialIDs: {
          $elemMatch: {
            id: profile.id,
            type: profile.provider,
          },
        },
      });
      if (!user || user.id !== userId) {
        return done(
          null,
          false,
          'Users did not match. Please sign in to specified account',
        );
      }
    } catch (error) {
      console.log(error);
      done(error, null);
    }
    done(null, user);
  };

  passport.use(
    new JwtStrategy(options, async (jwt_payload, done) => {
      try {
        const user = await User.findOne({ _id: jwt_payload.userId });
        if (user) return done(null, user);
        return done(null, false);
      } catch (error) {
        done(error, false);
      }
    }),
  );

  passport.use(
    'googleAuthentication',
    new GoogleStrategy(
      oauthStrategiesConfig.google.config,
      authenticationCallback,
    ),
  );
  passport.use(
    'facebookAuthentication',
    new FacebookStrategy(
      oauthStrategiesConfig.facebook.config,
      authenticationCallback,
    ),
  );
  passport.use(
    'stravaAuthentication',
    new StravaStrategy(
      oauthStrategiesConfig.strava.config,
      authenticationCallback,
    ),
  );
  passport.use(
    'googleAuthorization',
    new GoogleStrategy(
      oauthStrategiesConfig.google.config,
      authorizationCallback,
    ),
  );
  passport.use(
    'facebookAuthorization',
    new FacebookStrategy(
      oauthStrategiesConfig.facebook.config,
      authorizationCallback,
    ),
  );
  passport.use(
    'connectStrava',
    new StravaStrategy(
      oauthStrategiesConfig.connectStrava.config,
      connectStravaCallback,
    ),
  );
};
