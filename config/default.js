const includes = require('lodash/includes');

const url = includes(process.env.NODE_ENV, 'prod')
  ? 'https://racely.app'
  : `http://localhost:${process.env.PORT || 5000}/api`;

module.exports.url = url;

module.exports.oauthStrategiesConfig = {
  google: {
    config: {
      clientID: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
      passReqToCallback: true,
    },
    scope: ['email', 'profile'],
  },
  facebook: {
    config: {
      clientID: process.env.FACEBOOK_CLIENT_ID,
      clientSecret: process.env.FACEBOOK_CLIENT_SECRET,
      passReqToCallback: true,
      profileFields: [
        'emails',
        'first_name',
        'last_name',
        'displayName',
        'picture',
      ],
    },
    scope: ['email'],
  },
  strava: {
    config: {
      clientID: process.env.STRAVA_CLIENT_ID,
      clientSecret: process.env.STRAVA_CLIENT_SECRET,
      passReqToCallback: true,
    },
    scope: ['read_all,activity:read_all,profile:read_all'],
  },
  connectStrava: {
    config: {
      clientID: process.env.STRAVA_CLIENT_ID,
      clientSecret: process.env.STRAVA_CLIENT_SECRET,
      passReqToCallback: true,
    },
    scope: ['read_all,activity:read_all,profile:read_all'],
  },
};

module.exports.clientOrigin =
  process.env.NODE_ENV === 'production' ? '' : 'http://localhost:3000';

module.exports.jwtAuth = {
  accessTokenExpirationTime: 21600,
  refreshTokenExpirationTime: 604800,
};

module.exports.mongoURI = `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@racely-cluster-0-wagr0.gcp.mongodb.net/app?retryWrites=true&w=majority`;
