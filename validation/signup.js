const validator = require('validator');
const isEmpty = require('is-empty');

module.exports = function validateSignupInput(data) {
  const errors = {};

  if (
    typeof data.name !== 'string' ||
    typeof data.email !== 'string' ||
    typeof data.password !== 'string'
  ) {
    throw new Error(`Validation error. Registration data should be a string`);
  }

  if (validator.isEmpty(data.name)) {
    errors.name = 'Name field is required';
  }
  if (validator.isEmpty(data.email)) {
    errors.email = 'Email field is required';
  } else if (!validator.isEmail(data.email)) {
    errors.email = 'Email is invalid';
  }
  if (validator.isEmpty(data.password)) {
    errors.password = 'Password field is required';
  } else if (!validator.isLength(data.password, { min: 6, max: 30 })) {
    errors.password = 'Password must be at least 6 characters';
  }

  return {
    errors,
    isValid: isEmpty(errors),
  };
};
