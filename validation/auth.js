const validator = require('validator');
const isEmpty = require('is-empty');

module.exports = function validateAuthInput(data) {
  let errors = {};
  if (typeof data.email !== 'string' || typeof data.password !== 'string') {
    throw new Error(`Validation error. Registration data should be a string`);
  }

  if (validator.isEmpty(data.email)) {
    errors.email = 'Email field is required';
  } else if (!validator.isEmail(data.email)) {
    errors.email = 'Email is invalid';
  }
  if (validator.isEmpty(data.password)) {
    errors.password = 'Password field is required';
  }

  return {
    errors,
    isValid: isEmpty(errors),
  };
};
