const { Schema, model } = require('mongoose');

const userSchema = new Schema({
  emails: [{ type: String }],
  password: { type: String, default: null },
  firstName: { type: String, default: null },
  lastName: { type: String, default: null },
  profileImageUrl: { type: String, default: null },
  socialIDs: [{ id: { type: String }, type: { type: String } }],
  created: { type: Number, default: Date.now },
});

const accessTokenSchema = new Schema({
  userId: { type: String, required: true },
  accessToken: { type: String, required: true },
  expiresAt: { type: Number, required: true },
  apiType: { type: String, required: true },
});

const refreshTokenSchema = new Schema({
  userId: { type: String, required: true },
  refreshToken: { type: String, required: true },
  apiType: { type: String, required: true },
});

module.exports.User = model('User', userSchema);
module.exports.AccessToken = model('AccessToken', accessTokenSchema);
module.exports.RefreshToken = model('RefreshToken', refreshTokenSchema);
