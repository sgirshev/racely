require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const passport = require('passport');
const { mongoURI, clientOrigin } = require('./config/default');
const cors = require('cors');

require('./utils/redis').on('connect', () => {
  console.log('Successfully connected to Redis');
});

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

const corsOption = {
  origin: true,
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  credentials: true,
  exposedHeaders: ['x-auth-token'],
};
app.use(cors(corsOption));

app.use(passport.initialize());

const dbOptions = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: false,
};

async function start() {
  try {
    await mongoose.connect(mongoURI, dbOptions);
    console.log('Successfully connected to Database');
    app.listen(process.env.PORT, () => console.log(`App successfully started`));
  } catch (err) {
    console.log('Server error:', err.message);
  }
}

start();

require('./config/passport')(passport);

app.use('/api/auth', require('./routes/Auth'));
app.use('/api/athlete', require('./routes/Athlete'));
app.use('/api/activities', require('./routes/Activities'));
